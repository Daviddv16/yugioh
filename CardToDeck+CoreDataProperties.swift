//
//  CardToDeck+CoreDataProperties.swift
//  yugiohViewer
//
//  Created by David Vieth on 4/5/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import Foundation
import CoreData

extension CardToDeck {

    @NSManaged var amount: NSNumber?
    @NSManaged var card: Card?
    @NSManaged var deck: Deck?
}
