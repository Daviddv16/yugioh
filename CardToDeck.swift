//
//  CardToDeck.swift
//  yugiohViewer
//
//  Created by David Vieth on 4/5/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import Foundation
import CoreData

class CardToDeck: NSManagedObject {

    convenience init (card: Card, deck: Deck, amount: NSNumber = 1, context: NSManagedObjectContext) {
        if let ent = NSEntityDescription.entity(forEntityName: "CardToDeck", in: context) {
            self.init(entity: ent, insertInto: context)
            self.card = card
            self.deck = deck
            self.amount = amount
        } else {
            fatalError("Unable to find Entity name: Deck")
        }
    }
}
