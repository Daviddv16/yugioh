//
//  Card.swift
//  yugiohViewer
//
//  Created by David Vieth on 4/27/17.
//  Copyright © 2017 David Vieth. All rights reserved.
//

import UIKit
import CoreData

class Card: NSManagedObject {

    convenience init(card: CardData, context: NSManagedObjectContext) {
        if let ent = NSEntityDescription.entity(forEntityName: "Card", in: context) {
            self.init(entity: ent, insertInto: context)
            self.name = card.name
            self.cardType = card.cardType
            self.text = card.text

            if let monsterCard = card as? MonsterCard {
                self.type = monsterCard.type
                self.level = monsterCard.level as NSNumber
                self.attribute = monsterCard.attribute
                self.atk = monsterCard.atk as NSNumber
                self.def = monsterCard.def as NSNumber
                self.property = ""
            } else if let spellTrapCard = card as? SpellTrapCard {
                self.property = spellTrapCard.property.rawValue
            }
        } else {
            fatalError("Unable to find Entity name: Card")
        }
    }

    /// Returns the card data an an array of tuples(JSON Object)
    func getData() -> [String: AnyObject] {
        var cardData = [String: AnyObject]()

        cardData["name"] = self.name as AnyObject
        cardData["cardtype"] = self.cardType as AnyObject
        cardData["text"] = self.text as AnyObject

        if self.cardType == "monster" {
            cardData["type"] = self.type as AnyObject
            cardData["level"] = self.level
            cardData["attribute"] = self.attribute as AnyObject
            cardData["atk"] = self.atk
            cardData["def"] = self.def
        } else {
            cardData["property"] = self.property as AnyObject
        }

        return cardData
    }

    func toCardData() -> CardData? {
        if cardType == "monster" {
            return MonsterCard(data: getData())
        } else {
            return SpellTrapCard(data: getData())
        }
    }

    func getDeckRelation(for deck: Deck) -> CardToDeck? {
        return (decks.allObjects as? [CardToDeck])?.first(where: { $0.deck == deck })
    }
}
