//
//  AppDelegate.swift
//  flickr
//
//  Created by David Vieth on 3/27/17.
//  Copyright © 2017 David Vieth. All rights reserved.
//

//swiftlint:disable line_length

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    let stack = CoreDataStack(modelName: "Model")!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        stack.autoSave(30)

        if UserDefaults.standard.value(forKey: Config.Properties.showDetails.rawValue) == nil {
            Config.setShowDetailsProperty(to: true)
        }

        if UserDefaults.standard.value(forKey: Config.Properties.sortMethod.rawValue) == nil {
            Config.setSortMethodProperty(to: .byType)
        }

        if UserDefaults.standard.value(forKey: Config.Properties.sortDirection.rawValue) == nil {
            Config.setSortDirectionProperty(to: true)
        }

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.

        do {
            try stack.saveContext()
        } catch {
            print("Error while saving.")
        }
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.

        do {
            try stack.saveContext()
        } catch {
            print("Error while saving.")
        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}
