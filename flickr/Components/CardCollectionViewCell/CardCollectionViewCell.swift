//
//  CardCollectionViewCell.swift
//  yugiohViewer
//
//  Created by David Vieth on 3/29/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import UIKit
import Kingfisher

class CardCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cardImageView: UIImageView!

    /// Format CollectionViewCell based on card details
    func format(for card: CardData) {
        cardImageView.kf.setImage(with: APIRouter.getImage(name: card.name).url,
                                  placeholder: UIImage(named: "defaultCard"))
    }
}
