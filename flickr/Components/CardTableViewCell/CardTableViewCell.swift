//
//  CardTableViewCell.swift
//  yugiohViewer
//
//  Created by David Vieth on 3/26/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import UIKit
import Kingfisher

class CardTableViewCell: UITableViewCell {
    @IBOutlet weak var cardLevel: UILabel!
    @IBOutlet weak var cardName: UILabel!
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var starImage: UIImageView!
    @IBOutlet weak var attributeImage: UIImageView!
    @IBOutlet weak var atkDefLabel: UILabel!

    /// Format TableViewCell based on card details
    func format(for card: CardData, withDetails: Bool) {
        cardName.text = card.name

        cardName.textColor = UIColor.black
        atkDefLabel.textColor = UIColor.black
        cardLevel.textColor = UIColor.black

        cardImage.kf.setImage(with: APIRouter.getImage(name: card.name).url, placeholder: UIImage(named: "defaultCard"))

        if let cardData = card as? SpellTrapCard {
            backgroundColor = cardData.displayColor

            starImage.image = nil
            cardLevel.text = ""
            atkDefLabel.text = ""

            attributeImage.image = withDetails ? cardData.propertyIcon : nil
        } else if let cardData = card as? MonsterCard {
            backgroundColor = cardData.displayColor
            cardName.textColor = cardData.getTextColor()
            atkDefLabel.textColor = cardData.getTextColor()
            cardLevel.textColor = cardData.getTextColor()

            if withDetails {
                starImage.image = UIImage(named: "CG_Star")
                cardLevel.text = cardData.getLevel()
                attributeImage.image = cardData.getAttributeImage()
                atkDefLabel.text = cardData.getAtkDef()
            } else {
                starImage.image = nil
                cardLevel.text = ""
                attributeImage.image = nil
                atkDefLabel.text = ""
            }
        } else {
            print("Error: Unable to convert card to proper format: \(card.cardType)")
        }

        return
    }
}
