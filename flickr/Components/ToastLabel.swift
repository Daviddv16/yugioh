//
//  ToastLabel.swift
//  yugiohViewer
//
//  Created by David Vieth on 1/15/18.
//  Copyright © 2018 David Vieth. All rights reserved.
//

import UIKit

class ToastLabel: UILabel {
    override open func layoutSubviews() {
        super.layoutSubviews()

        backgroundColor = UIColor.black.withAlphaComponent(0.75)
        textColor = .white
        textAlignment = .center
        font = UIFont(name: "Montserrat-Light", size: 12.0)
        alpha = 1.0
        layer.cornerRadius = 10
        clipsToBounds = true
    }
}
