//
//  NavBarView.swift
//  yugiohViewer
//
//  Created by David Vieth on 1/26/18.
//  Copyright © 2018 David Vieth. All rights reserved.
//

import UIKit

/// Delegate to communnicate with parent view
protocol NavBarViewDelegate: class {
	func didClickBackButton()
	func didClickPlusButton()
}

extension NavBarViewDelegate {
	func didClickPlusButton() {
		return
	}
}

class NavBarView: UIView {
	// MARK: Outlets

	@IBOutlet var contentView: UIView!
	@IBOutlet weak var backBtnStackView: UIStackView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var plusButton: UIButton!

	// MARK: Properties

	weak var delegate: NavBarViewDelegate?

	// MARK: UIView

	override init(frame: CGRect) {
		super.init(frame: frame)
		commonInit()
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		commonInit()
	}

	func commonInit() {
		Bundle.main.loadNibNamed("NavBarView", owner: self, options: nil)
		addSubview(contentView)
		contentView.frame = self.bounds
		contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]

		let backTap = UITapGestureRecognizer(target: self, action: #selector(pressBackBtn))
		backBtnStackView.addGestureRecognizer(backTap)

        plusButton.setFontAwesomeIcon(to: .plus)
	}

    @objc func pressBackBtn(sender: UITapGestureRecognizer) {
        let animationDuration = 0.1

        UIStackView.animate(withDuration: animationDuration,
                            animations: {() -> Void in
                                sender.view?.alpha = 0.25
                            },
                            completion: {_ -> Void in
                                UIStackView.animate(withDuration: animationDuration,
                                                    delay: 0.25,
                                                    options: .curveEaseIn,
                                                    animations: { () -> Void in
                                                        sender.view?.alpha = 1
                                                    },
                                                    completion: nil)
                            })

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.25) { [weak self] in
            self?.delegate?.didClickBackButton()
        }
    }

	@objc func pressPlusBtn() {
		delegate?.didClickPlusButton()
	}

	func showPlusButton() {
		plusButton.isHidden = false
		plusButton.isEnabled = true

		let plusTap = UITapGestureRecognizer(target: self, action: #selector(pressPlusBtn))
		plusButton.addGestureRecognizer(plusTap)
	}
}
