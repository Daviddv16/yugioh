//
//  CardDetailsStackView.swift
//  yugiohViewer
//
//  Created by David Vieth on 4/6/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import UIKit

class CardDetailsStackView: UIStackView {
    // MARK: Outlets

    @IBOutlet var contentView: UIStackView!
    @IBOutlet weak var attackLabel: UILabel?
    @IBOutlet weak var defenceLabel: UILabel?
    @IBOutlet weak var typeLabel: UILabel?
    @IBOutlet weak var levelLabel: UILabel?
    @IBOutlet weak var typesLabel: UILabel?

    // MARK: UIView

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    func commonInit() {
        Bundle.main.loadNibNamed("CardDetailsStackView", owner: self, options: nil)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.frame = frame
        addSubview(contentView)

        NSLayoutConstraint.activate([
            contentView.leadingAnchor.constraint(equalTo: leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: trailingAnchor),
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }

    func setDetails(for card: CardData) {
        if let card = card as? MonsterCard {
            attackLabel?.text  = "ATK:\n" + String(describing: card.atk)
            defenceLabel?.text = "DEF:\n" + String(describing: card.def)
            typeLabel?.text = card.attribute.capitalized
            levelLabel?.text = "Level:\n" + String(describing: card.level)
            typesLabel?.text =  card.type.split(separator: "/").map(String.init)[0]
        } else {
            attackLabel?.text = ""
            defenceLabel?.text = ""
            levelLabel?.text = ""
            typeLabel?.text = ""
            typesLabel?.text = ""
        }
    }
}
