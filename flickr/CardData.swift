//
//  CardData.swift
//  yugiohViewer
//
//  Created by David Vieth on 1/16/18.
//  Copyright © 2018 David Vieth. All rights reserved.
//

import UIKit

class CardData {
    var name: String
    var cardType: String
    var text: String

    init(data: CardDataDecodable) {
        self.name = data.name
        self.cardType = data.cardType
        self.text = data.text
    }

    init(name: String, cardType: String, text: String) {
        self.name = name
        self.cardType = cardType
        self.text = text
    }

    /// Get String indicating which decks the card is in and how many times it's in that deck
    func getDeckList() -> String {
        let delegate = UIApplication.shared.delegate as? AppDelegate
        let stack = delegate?.stack

        do {
            let cards = try stack?.context.fetch(Card.createFetchRequest())

            guard let card = cards?.first(where: { $0.name == name }),
                  let decks = (card.decks.allObjects as? [CardToDeck]) else {
                return ""
            }

            var message = ""
            for item in decks {
                if let name = item.deck?.name, let count = item.amount {
                    message += "\(name) (x\(count)),\n"
                }
            }
            let index = message.index(message.endIndex, offsetBy: -2)
            return String(message[..<index])
        } catch {
            let fetchError = error as NSError
            print(fetchError)
            return ""
        }
    }

    func getCoreDataCard() -> Card? {
        let delegate = UIApplication.shared.delegate as? AppDelegate
        let stack = delegate?.stack

        do {
            let cards = try stack?.context.fetch(Card.createFetchRequest())
            return cards?.first(where: { $0.name == name })
        } catch {
            let fetchError = error as NSError
            print(fetchError)
            return nil
        }
    }
}

class MonsterCard: CardData {
    var type: String
    var level: Int
    var attribute: String
    var atk: Int
    var def: Int

    /// Determine if the card belongs in the extra deck or not
    var isExtraDeck: Bool {
        type.contains("Fusion") || type.contains("Synchro") || type.contains("Xyz") || type.contains("Link")
    }

    /// Getter for extra type
    var extraType: String? {
        isExtraDeck ? type.split(separator: "/").map(String.init)[1] : nil
    }

    var displayColor: UIColor {
        if type.contains("Fusion") {
            return Colors.fusionPurple
        } else if type.contains("Synchro") {
            return Colors.synchroSilver
        } else if type.contains("Xyz") {
            return Colors.xyzBlack
        } else if type.contains("Ritual") {
            return Colors.ritualBlue
        } else if type.contains("Link") {
            return Colors.linkBlue
        } else if type.contains("Effect") {
            return Colors.effectOrange
        } else {
            return Colors.normalYellow
        }
    }

    override init(data: CardDataDecodable) {
        self.type = data.type ?? ""
        self.level = data.level ?? 0
        self.attribute = data.attribute ?? ""
        self.atk = data.atk ?? 0
        self.def = data.def ?? 0

        super.init(data: data)
    }

    init?(data: [String: AnyObject]) {
        guard let name = data["name"] as? String else {
            print("Failed to set card name")
            return nil
        }
        guard let cardType = data["cardtype"] as? String else {
            print("Failed to set card cardtype")
            return nil
        }
        guard let text = data["text"] as? String else {
            print("Failed to set card text")
            return nil
        }
        guard let type = data["type"] as? String else {
            print("Failed to set card type")
            return nil
        }
        guard let level = data["level"] as? NSNumber else {
            print("Failed to set card level")
            return nil
        }
        guard let attribute = data["attribute"] as? String else {
            print("Failed to set card attribute")
            return nil
        }
        guard let atk = data["atk"] as? NSNumber else {
            print("Failed to set card atk")
            return nil
        }
        guard let def = data["def"] as? NSNumber else {
            print("Failed to set card def")
            return nil
        }

        self.type = type
        self.level = level.intValue
        self.attribute = attribute
        self.atk = atk.intValue
        self.def = def.intValue
        super.init(name: name, cardType: cardType, text: text)
    }

    func getTextColor() -> UIColor {
        if type.contains("Xyz") {
            return Colors.xyzGold
        } else if type.contains("Link") {
            return .white
        } else {
            return Colors.text
        }
    }

    func getLevel() -> String {
        return "x" + String(format: "%02d", level)
    }

    func getAttributeImage() -> UIImage? {
        return UIImage(named: attribute)
    }

    func getAtkDef() -> String {
        return String(describing: atk) + " / " + String(describing: def)
    }
}

class SpellTrapCard: CardData {
    enum Properties: String, CaseIterable {
        case normal = "Normal"
        case continuous = "Continuous"
        case equip = "Equip"
        case field = "Field"
        case quick = "Quick-Play"
        case ritual = "Ritual"
        case counter = "Counter"
    }

    var property: Properties

    var propertyIcon: UIImage? {
        UIImage(named: property.rawValue)
    }

    var displayColor: UIColor {
        return cardType == "trap" ? Colors.trapPurple : Colors.spellGreen
    }

    override init(data: CardDataDecodable) {
        self.property = Properties(rawValue: data.property ?? "") ?? .normal
        super.init(data: data)
    }

    init?(data: [String: AnyObject]) {
        guard let name = data["name"] as? String else {
            return nil
        }
        guard let cardType = data["cardtype"] as? String else {
            return nil
        }
        guard let text = data["text"] as? String else {
            return nil
        }
        guard let property = data["property"] as? String else {
            return nil
        }

        self.property = Properties(rawValue: property) ?? .normal
        super.init(name: name, cardType: cardType, text: text)
    }
}
