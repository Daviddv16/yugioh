//
//  Deck+CoreDataProperties.swift
//  yugiohViewer
//
//  Created by David Vieth on 4/27/17.
//  Copyright © 2017 David Vieth. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Deck {

    @NSManaged var name: String
    @NSManaged var cards: NSSet

}
