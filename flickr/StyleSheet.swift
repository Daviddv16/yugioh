//
//  StyleSheet.swift
//  yugiohViewer
//
//  Created by David Vieth on 3/25/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import UIKit

struct Colors {
    static let fusionPurple = UIColor(hexCode: "#8C478A")
    static let synchroSilver = UIColor(hexCode: "#BEBEBE")
    static let xyzBlack = UIColor(hexCode: "#191957")
    static let xyzGold = UIColor(hexCode: "#FFD600")
    static let ritualBlue = UIColor(hexCode: "#4169E1")
    static let effectOrange = UIColor(hexCode: "#FFa61c")
    static let linkBlue = UIColor(hexCode: "#00008B")
    static let normalYellow = UIColor(hexCode: "#FFd41c")

    static let trapPurple = UIColor(hexCode: "#BF00BF")
    static let spellGreen = UIColor(hexCode: "#0DAAA0")

    static let text = UIColor.black
}
