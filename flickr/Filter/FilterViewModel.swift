//
//  FilterViewModel.swift
//  yugiohViewer
//
//  Created by David Vieth on 4/8/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import Foundation

protocol FilterViewModelDelegate: class {
    func onApply(filters: [Filter])
}

class FilterViewModel {

    // MARK: Properties

    var filters: [Filter]
    let title: String

    weak var delegate: FilterViewModelDelegate?

    init(filters: [Filter], title: String) {
        self.filters = filters
        self.title = title
    }

    func applyFilters() {
        delegate?.onApply(filters: filters)
    }
}
