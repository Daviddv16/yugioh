//
//  FilterRangeViewModel.swift
//  yugiohViewer
//
//  Created by David Vieth on 4/14/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import Foundation

protocol FilterRangeViewModelDelegate: class {
    func onApply(filter: FilterRange)
}

class FilterRangeViewModel {

    // MARK: Properties

    var filter: FilterRange
    weak var delegate: FilterRangeViewModelDelegate?

    init(filter: FilterRange) {
        self.filter = filter
    }

    func applyFilters() {
        delegate?.onApply(filter: filter)
    }
}
