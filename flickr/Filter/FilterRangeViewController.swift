//
//  FilterRangeViewController.swift
//  yugiohViewer
//
//  Created by David Vieth on 4/14/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import UIKit

class FilterRangeViewController: UIViewController {

    // MARK: Outlets

    @IBOutlet weak var navBarView: NavBarView!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var minimumSlider: UISlider!
    @IBOutlet weak var maximumSlider: UISlider!
    @IBOutlet weak var enabledSwitch: UISwitch!

    // MARK: Actions

    @IBAction func onTouchUpInsideApplyButton(_ sender: Any) {
        viewModel.applyFilters()
        navigationController?.popViewController(animated: true)
    }
    @IBAction func minimumSliderValueChanged(_ sender: Any) {
        headerLabel.text = "\(round(minimumSlider.value)) - \(round(maximumSlider.value))"
        viewModel.filter.minimum = round(minimumSlider.value)
    }
    @IBAction func maximumSliderValueChanged(_ sender: Any) {
        headerLabel.text = "\(round(minimumSlider.value)) - \(round(maximumSlider.value))"
        viewModel.filter.maximum = round(maximumSlider.value)
    }
    @IBAction func enabledSwitchValueChanged(_ sender: UISwitch) {
        viewModel.filter.isEnabled = sender.isOn
        minimumSlider.isEnabled = sender.isOn
        maximumSlider.isEnabled = sender.isOn

        if sender.isOn && (viewModel.filter.minimum == nil || viewModel.filter.minimum == nil) {
            setDefaultValues()
        } else if !sender.isOn {
            headerLabel.text = " - "
        }
    }

    // MARK: Properties

    var viewModel: FilterRangeViewModel!

    // MARK: UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        navBarView.delegate = self
        navBarView.showPlusButton()
        navBarView.plusButton.setTitle("Clear", for: .normal)
        navBarView.plusButton.titleLabel?.font = .systemFont(ofSize: 20.0)

        setView()
    }

    func setView() {
        enabledSwitch.setOn(viewModel.filter.isEnabled, animated: false)
        minimumSlider.isEnabled = viewModel.filter.isEnabled
        maximumSlider.isEnabled = viewModel.filter.isEnabled

        if let min = viewModel.filter.minimum, let max = viewModel.filter.maximum {
            minimumSlider.setValue(Float(min), animated: false)
            maximumSlider.setValue(Float(max), animated: false)
            headerLabel.text = "\(min) - \(max)"
        } else if viewModel.filter.isEnabled {
            setDefaultValues()
        } else {
            headerLabel.text = " - "
        }
    }

    func setDefaultValues() {
        let min = round(minimumSlider.minimumValue)
        let max = round(maximumSlider.maximumValue)
        minimumSlider.setValue(minimumSlider.minimumValue, animated: false)
        maximumSlider.setValue(maximumSlider.maximumValue, animated: false)

        viewModel.filter.minimum = min
        viewModel.filter.maximum = max

        headerLabel.text = "\(min) - \(max)"
    }

    func round(_ value: Float) -> Int {
        return Int(value / 100) * 100
    }
}

// MARK: NavBarViewDelegate

extension FilterRangeViewController: NavBarViewDelegate {
    func didClickBackButton() {
        navigationController?.popViewController(animated: true)
    }

    func didClickPlusButton() {
        viewModel.filter.clearFilters()
        setView()
    }
}
