//
//  FilterOptionTableViewCell.swift
//  yugiohViewer
//
//  Created by David Vieth on 4/7/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import UIKit

class FilterOptionTableViewCell: UITableViewCell {

    // MARK: Outlets

    @IBOutlet weak var headerLabel: UILabel!

    // MARK: Properties

    func setDisplay(for option: (String, Bool)) {
        headerLabel.text = option.0
        tintColor = option.1 ? .orange : .gray
    }
}
