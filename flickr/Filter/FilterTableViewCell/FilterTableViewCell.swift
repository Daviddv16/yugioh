//
//  FilterTableViewCell.swift
//  Yu-Gi-Oh ViewerTests
//
//  Created by David Vieth on 4/7/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {

    // MARK: Outlets

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var selectedLabel: UILabel!
    @IBOutlet weak var selectedCountBackgroundView: UIView!
    @IBOutlet weak var booleanSwitch: UISwitch!

    override func layoutSubviews() {
        super.layoutSubviews()
        selectedCountBackgroundView.layer.cornerRadius = selectedCountBackgroundView.frame.height / 2
    }

    // MARK: Setters

    func setDisplay(for filter: Filter) {
        headerLabel.text = filter.displayName
        selectedLabel.text = filter.selectedName

        if filter is FilterRange {
            booleanSwitch.isHidden = true
            selectedLabel.isHidden = false
            selectedCountBackgroundView.isHidden = true
            selectedLabel.textColor = Colors.text
            accessoryType = .disclosureIndicator
        } else if let optionsFilter = filter as? FilterOptions {
            if optionsFilter.type == .multiSelect, optionsFilter.selectedName != nil {
                selectedCountBackgroundView.isHidden = false
                selectedLabel.textColor = .white
            } else {
                selectedCountBackgroundView.isHidden = true
                selectedLabel.textColor = Colors.text
            }

            if optionsFilter.type == .boolean {
                booleanSwitch.setOn(optionsFilter.options.first?.1 ?? false, animated: false)
                booleanSwitch.isHidden = false
                selectedLabel.isHidden = true
                accessoryType = .none
            } else {
                booleanSwitch.isHidden = true
                selectedLabel.isHidden = false
                accessoryType = .disclosureIndicator
            }
        }
    }
}
