//
//  FilterOptionsViewModel.swift
//  yugiohViewer
//
//  Created by David Vieth on 4/8/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import Foundation

protocol FilterOptionsViewModelDelegate: class {
    func onApply(filter: FilterOptions)
}

class FilterOptionsViewModel {

    // MARK: Properties

    var filter: FilterOptions
    weak var delegate: FilterOptionsViewModelDelegate?

    init(filter: FilterOptions) {
        self.filter = filter
    }

    func applyFilters() {
        delegate?.onApply(filter: filter)
    }
}
