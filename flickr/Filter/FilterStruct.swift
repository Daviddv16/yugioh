//
//  FilterStruct.swift
//  yugiohViewer
//
//  Created by David Vieth on 4/8/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import Foundation

enum FilterType {
    case singleSelect
    case singleSelectRequired
    case boolean
    case multiSelect
}

protocol Filter {
    var displayName: String { get set }
    var selectedName: String? { get }
    mutating func clearFilters()
}

struct FilterOptions: Filter {
    var displayName: String
    var options: [(String, Bool)]
    var defaultSelectedIndex: Int?
    var type: FilterType
}

extension FilterOptions {
    var selectedName: String? {
        switch type {
        case .singleSelect, .singleSelectRequired:
            return options.first(where: { $0.1 })?.0
        case .multiSelect:
            let count = options.filter({ $0.1 }).count
            return count > 0 ? "\(count)" : nil
        case .boolean:
            return nil
        }
    }

    mutating func clearFilters() {
        for index in 0..<options.count {
            options[index].1 = index == defaultSelectedIndex
        }
    }

    mutating func toggleOption(at index: Int) {
        switch type {
        case .singleSelect:
            if let currentIndex = options.firstIndex(where: { $0.1 }), currentIndex != index {
                options[currentIndex].1.toggle()
            }
            options[index].1.toggle()
        case .singleSelectRequired:
            if let currentIndex = options.firstIndex(where: { $0.1 }) {
                if currentIndex != index {
                    options[currentIndex].1.toggle()
                    options[index].1.toggle()
                }
            } else {
                options[index].1.toggle()
            }
        case .multiSelect, .boolean:
            options[index].1.toggle()
        }
    }
}

struct FilterRange: Filter {
    var displayName: String
    var minimum: Int?
    var maximum: Int?
    var isEnabled: Bool = false
}

extension FilterRange {
    var selectedName: String? {
        if isEnabled, let min = minimum, let max = maximum {
            return "\(min) - \(max)"
        }

        return nil
    }

    mutating func clearFilters() {
        minimum = nil
        maximum = nil
        isEnabled = false
    }
}
