//
//  FilterViewController.swift
//  Yu-Gi-Oh ViewerTests
//
//  Created by David Vieth on 4/7/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import UIKit

class FilterViewController: FilterParentViewController {

    // MARK: Properties

    var selectedIndex: Int?

    var viewModel: FilterViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        filterDelegate = self

        navBarView.delegate = self
        navBarView.titleLabel.text = viewModel.title

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "FilterTableViewCell", bundle: nil),
                           forCellReuseIdentifier: "FilterTableViewCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 54
    }
}

// MARK: UITableViewDelegate, UITableViewDataSource

extension FilterViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.filters.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCell")
                as? FilterTableViewCell else {
            return UITableViewCell()
        }

        cell.setDisplay(for: viewModel.filters[indexPath.row])

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        if var optionsFilter = viewModel.filters[indexPath.row] as? FilterOptions {
            if optionsFilter.type == .boolean {
                if let cell = tableView.cellForRow(at: indexPath) as? FilterTableViewCell {
                    cell.booleanSwitch.setOn(!cell.booleanSwitch.isOn, animated: true)
                }
                optionsFilter.toggleOption(at: 0)
                viewModel.filters[indexPath.row] = optionsFilter
            } else {
                (navigationController as? NavController)?.navigateToFilterOptions(with: optionsFilter,
                                                                                  from: self)
            }
        } else if let rangeFilter = viewModel.filters[indexPath.row] as? FilterRange {
                (navigationController as? NavController)?.navigateToFilterRange(with: rangeFilter,
                                                                                  from: self)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: NavBarViewDelegate

extension FilterViewController: NavBarViewDelegate {
    func didClickBackButton() {
        navigationController?.popViewController(animated: true)
    }

    func didClickPlusButton() {
        for index in 0..<viewModel.filters.count {
            viewModel.filters[index].clearFilters()
        }
        tableView.reloadData()
    }
}

// MARK: FilterOptionsViewModelDelegate

extension FilterViewController: FilterOptionsViewModelDelegate {
    func onApply(filter: FilterOptions) {
        if let selectedIndex = selectedIndex {
            viewModel.filters[selectedIndex] = filter
            tableView.reloadData()
        }
    }
}

// MARK: FilterRangeViewModelDelegate

extension FilterViewController: FilterRangeViewModelDelegate {
    func onApply(filter: FilterRange) {
        if let selectedIndex = selectedIndex {
            viewModel.filters[selectedIndex] = filter
            tableView.reloadData()
        }
    }
}

// MARK: FilterParentViewControllerDelegate

extension FilterViewController: FilterParentViewControllerDelegate {
    func onApplyButtonPress() {
        viewModel.applyFilters()
        navigationController?.popViewController(animated: true)
    }
}
