//
//  FilterOptionsViewController.swift
//  Yu-Gi-Oh ViewerTests
//
//  Created by David Vieth on 4/7/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import UIKit

class FilterOptionsViewController: FilterParentViewController {

    // MARK: Properties

    var viewModel: FilterOptionsViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        filterDelegate = self

        navBarView.delegate = self
        navBarView.titleLabel.text = viewModel.filter.displayName

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "FilterOptionTableViewCell", bundle: nil),
                           forCellReuseIdentifier: "FilterOptionTableViewCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 54
    }

}

// MARK: UITableViewDelegate, UITableViewDataSource

extension FilterOptionsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.filter.options.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FilterOptionTableViewCell")
                as? FilterOptionTableViewCell else {
            return UITableViewCell()
        }

        cell.setDisplay(for: viewModel.filter.options[indexPath.row])

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.filter.toggleOption(at: indexPath.row)
        tableView.reloadData()
    }
}

// MARK: NavBarViewDelegate

extension FilterOptionsViewController: NavBarViewDelegate {
    func didClickBackButton() {
        navigationController?.popViewController(animated: true)
    }

    func didClickPlusButton() {
        viewModel.filter.clearFilters()
        tableView.reloadData()
    }
}

// MARK: FilterParentViewControllerDelegate

extension FilterOptionsViewController: FilterParentViewControllerDelegate {
    func onApplyButtonPress() {
        viewModel.applyFilters()
        navigationController?.popViewController(animated: true)
    }
}
