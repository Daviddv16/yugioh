//
//  FilterParentViewController.swift
//  yugiohViewer
//
//  Created by David Vieth on 4/7/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import UIKit

protocol FilterParentViewControllerDelegate: class {
    func onApplyButtonPress()
}

class FilterParentViewController: UIViewController {

    // MARK: Outlets

    @IBOutlet weak var navBarView: NavBarView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var applyButton: UIButton!

    @IBAction func onTouchUpInsideApplyButton(_ sender: Any) {
        filterDelegate?.onApplyButtonPress()
    }

    // MARK: Properties

    weak var filterDelegate: FilterParentViewControllerDelegate?

    // MARK: UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        navBarView.showPlusButton()
        navBarView.plusButton.setTitle("Clear", for: .normal)
        navBarView.plusButton.titleLabel?.font = .systemFont(ofSize: 20.0)
    }
}
