//
//  Card+CoreDataProperties.swift
//  yugiohViewer
//
//  Created by David Vieth on 4/27/17.
//  Copyright © 2017 David Vieth. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Card {

    public class func createFetchRequest() -> NSFetchRequest<Card> {
        let fetchRequest = NSFetchRequest<Card>(entityName: "Card")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        return fetchRequest
    }

    @NSManaged var atk: NSNumber?
    @NSManaged var attribute: String?
    @NSManaged var cardType: String
    @NSManaged var def: NSNumber?
    @NSManaged var level: NSNumber?
    @NSManaged var name: String
    @NSManaged var text: String
    @NSManaged var type: String?
    @NSManaged var decks: NSSet
    @NSManaged var property: String?

}
