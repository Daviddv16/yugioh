//
//  navViewController.swift
//  yugiohViewer
//
//  Created by David Vieth on 5/15/17.
//  Copyright © 2017 David Vieth. All rights reserved.
//

import UIKit

class NavViewController: UIViewController {
    // MARK: Outlets

    @IBOutlet weak var viewDeckStack: UIStackView!
    @IBOutlet weak var viewCardsStack: UIStackView!
    @IBOutlet weak var searchCardsStack: UIStackView!
    @IBOutlet weak var moreInfoStack: UIStackView!

    // MARK: UIView
    override func viewDidLoad() {
        super.viewDidLoad()

        let tapDeck = UITapGestureRecognizer(target: self, action: #selector(NavViewController.navAway))
        let tapCards = UITapGestureRecognizer(target: self, action: #selector(NavViewController.navAway))
        let tapSearch = UITapGestureRecognizer(target: self, action: #selector(NavViewController.navAway))
        let tapInfo = UITapGestureRecognizer(target: self, action: #selector(NavViewController.navAway))

        viewDeckStack.addGestureRecognizer(tapDeck)
        viewCardsStack.addGestureRecognizer(tapCards)
        searchCardsStack.addGestureRecognizer(tapSearch)
        moreInfoStack.addGestureRecognizer(tapInfo)
    }

    /// navigate to controller
    @objc func navAway(sender: UITapGestureRecognizer) {
        let animationDuration = 0.1
        UIStackView.animate(withDuration: animationDuration,
                            animations: {() -> Void in
                                sender.view?.alpha = 0.25
                            }, completion: {_ -> Void in
                                UIStackView.animate(withDuration: animationDuration,
                                                    delay: 0.25,
                                                    options: .curveEaseIn,
                                                    animations: { () -> Void in
                                                        sender.view?.alpha = 1
                                                    }, completion: {[weak self] _ -> Void in
                                                        self?.navigate(to: sender.view?.tag ?? -1)
                                                    })
                            })
    }

    func navigate(to index: Int) {
        guard let navController = navigationController as? NavController else {
            return
        }

        switch index {
        case 1:
            navController.navigateToDecks()
        case 2:
            navController.navigateToViewCards()
        case 3:
            navController.navigateToViewCardDetails()
        case 4:
            navController.navigateToMoreInfo()
        default:
            break
        }
    }
}
