//
//  AllDecksTableViewController.swift
//  Yu-Gi-Oh Card Finder
//
//  Created by David Vieth on 3/21/17.
//  Copyright © 2017 David Vieth. All rights reserved.
//

import UIKit
import CoreData

class AllDecksTableViewController: UIViewController {
    // MARK: Outlets

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navBarView: NavBarView!

    // MARK: Properties

    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>? {
        didSet {
            // Whenever the frc changes, we execute the search and
            // reload the table
            fetchedResultsController?.delegate = self
            executeSearch()
            tableView.reloadData()
        }
    }

    // MARK: UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        navBarView.delegate = self
        navBarView.titleLabel.text = "Deck List"
        navBarView.showPlusButton()

        guard let delegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let stack = delegate.stack

        // Create a fetchrequest
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Deck")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]

        // Create the FetchedResultsController
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                              managedObjectContext: stack.context,
                                                              sectionNameKeyPath: nil,
                                                              cacheName: nil)

        let longpress = UILongPressGestureRecognizer(target: self, action: #selector(longPressGestureRecognized))
        tableView.addGestureRecognizer(longpress)
    }

    /// fetch decks from database
    func executeSearch() {
        if let fetchController = fetchedResultsController {
            do {
                try fetchController.performFetch()
            } catch let error as NSError {
                let errorDescription = String(describing: fetchedResultsController)
                print("Error while trying to perform a search: \n\(error)\n\(errorDescription)")
            }
        }
    }

    /// Creates alert controller with options for handling deck/table
    @objc func longPressGestureRecognized(_ sender: UIGestureRecognizer) {
        guard let longPress = sender as? UILongPressGestureRecognizer else {
            return
        }
        let index = longPress.location(in: tableView)
        let indexPath = tableView.indexPathForRow(at: index)! as IndexPath

        if sender.state == .began {
            // 1 Make Action Sheet
            let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)

            // 2 Create Custom Actions
            let nameAction = UIAlertAction(title: "Change Name", style: .default) {(alert: UIAlertAction!) -> Void in
                let object = self.fetchedResultsController!.object(at: indexPath) as? NSManagedObject

                let alertController = UIAlertController(title: "New Name",
                                                        message: "Please input Deck Name:",
                                                        preferredStyle: .alert)

                let confirmAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
                    let field = alertController.textFields![0]
                    if field.text != "" {
                        object?.setValue(field.text, forKeyPath: "name")
                    }
                }

                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }

                alertController.addTextField { (textField) in
                    textField.placeholder = "New Deck"
                }

                alertController.addAction(confirmAction)
                alertController.addAction(cancelAction)

                self.present(alertController, animated: true, completion: nil)
            }

            let deleteAction = UIAlertAction(title: "Remove Deck", style: .default) {_ -> Void in
                self.deleteItem(indexPath)
            }

            //3 Create Cancel Action
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {_ -> Void in
                print("Cancelled")
            }

            // 4 Add Actions
            optionMenu.addAction(nameAction)
            optionMenu.addAction(deleteAction)
            optionMenu.addAction(cancelAction)

            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
                let cell = tableView(tableView, cellForRowAt: indexPath)

                optionMenu.modalPresentationStyle = UIModalPresentationStyle.popover
                let popPresenter: UIPopoverPresentationController = optionMenu.popoverPresentationController!
                popPresenter.sourceView = tableView
                popPresenter.sourceRect = cell.frame
            }

            // 5 Display Action Sheet
            self.present(optionMenu, animated: true, completion: nil)
        }
    }

    /// Create new deck from name and save to database
    func saveDeck(_ name: String) {
        guard let delegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let stack = delegate.stack

        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Deck")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        let fetchedResultsController: NSFetchedResultsController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: stack.context,
            sectionNameKeyPath: nil,
            cacheName: nil
        )

        _ = Deck(name: name, context: fetchedResultsController.managedObjectContext)
    }

    /// Delete deck from database and reload tableView
    func deleteItem(_ indexPath: IndexPath) {
        let object = fetchedResultsController!.object(at: indexPath) as? NSManagedObject

        fetchedResultsController?.managedObjectContext.delete(object!)
    }
}

// MARK: - NavBarViewDelegate

extension AllDecksTableViewController: NavBarViewDelegate {
    func didClickBackButton() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }

    func didClickPlusButton() {
        let alertController = UIAlertController(title: "New Deck",
                                                message: "Please input Deck Name:",
                                                preferredStyle: .alert)

        let confirmAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
            let field = alertController.textFields![0]
            if let fieldText = field.text, fieldText != "" {
                self.saveDeck(fieldText)
            } else {
                print("user did not fill field")
            }
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }

        alertController.addTextField { (textField) in
            textField.placeholder = "New Deck"
        }

        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }
}

// MARK: - UITableViewDelegate

extension AllDecksTableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let deck = fetchedResultsController?.object(at: indexPath) as? Deck
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "DeckCell2")

        cell.textLabel?.text = deck?.name ?? ""
        cell.detailTextLabel?.text = "x\(deck?.amount ?? 0)"

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Constants.Misc.deckIndex = indexPath.row

        (navigationController as? NavController)?.navigateToDeck()
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - UITableViewDataSource

extension AllDecksTableViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if let fetchController = fetchedResultsController {
            return (fetchController.sections?.count)!
        } else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let fetchController = fetchedResultsController {
            return fetchController.sections?[section].numberOfObjects ?? 0
        } else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let fetchController = fetchedResultsController {
            return fetchController.sections![section].name
        } else {
            return nil
        }
    }

    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        if let fetchController = fetchedResultsController {
            return fetchController.section(forSectionIndexTitle: title, at: index)
        } else {
            return 0
        }
    }

    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if let fetchController = fetchedResultsController {
            return fetchController.sectionIndexTitles
        } else {
            return nil
        }
    }
}

// MARK: - NSFetchedResultsControllerDelegate

extension AllDecksTableViewController: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange sectionInfo: NSFetchedResultsSectionInfo,
                    atSectionIndex sectionIndex: Int,
                    for type: NSFetchedResultsChangeType) {
        let set = IndexSet(integer: sectionIndex)

        switch type {
        case .insert:
            tableView.insertSections(set, with: .fade)
        case .delete:
            tableView.deleteSections(set, with: .fade)
        default:
            break
        }
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .fade)
        case .move:
            tableView.deleteRows(at: [indexPath!], with: .fade)
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        @unknown default:
            print("Unknown case not handled")
        }
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}
