//
//  NavController.swift
//  Yu-Gi-Oh ViewerTests
//
//  Created by David Vieth on 4/1/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import UIKit

class NavController: UINavigationController {
    func navigateToViewCardDetails(deck: Deck? = nil, cards: [CardData]? = nil, displayIndex: Int? = nil) {
        let controller = storyboard?.instantiateViewController(withIdentifier: "ViewCardDetailsViewController")
            as? ViewCardDetailsViewController
        controller?.viewModel = ViewCardDetailsViewModel(deck: deck, cards: cards, displayIndex: displayIndex)

        if let controller = controller {
            pushViewController(controller, animated: true)
        }
    }

    func navigateToDecks() {
        let controller = storyboard?.instantiateViewController(withIdentifier: "ViewDecks")
            as? AllDecksTableViewController

        if let controller = controller {
            pushViewController(controller, animated: true)
        }
    }

    func navigateToDeck() {
        let controller = storyboard?.instantiateViewController(withIdentifier: "ViewDeck")
            as? DeckTableViewController
        controller?.viewModel = CardTableViewModel()

        if let controller = controller {
            pushViewController(controller, animated: true)
        }
    }

    func navigateToViewCards() {
        let controller = storyboard?.instantiateViewController(withIdentifier: "ViewCards") as? UITabBarController

        if let controller = controller {
            for viewController in controller.viewControllers ?? [] {
                if (viewController as? CardTableViewController) != nil {
                    (viewController as? CardTableViewController)?.viewModel = CardTableViewModel()
                } else {
                    (viewController as? CardsDisplayViewController)?.viewModel = CardsDisplayViewModel()
                }
            }
            pushViewController(controller, animated: true)
        }
    }

    func navigateToMoreInfo() {
        let controller = storyboard?.instantiateViewController(withIdentifier: "MoreInfo")

        if let controller = controller {
            pushViewController(controller, animated: true)
        }
    }

    func navigateToFilter(with filters: [Filter], _ title: String, from prevController: FilterViewModelDelegate) {
        let filterStoryboard = UIStoryboard(name: "Filter", bundle: nil)
        if let controller = filterStoryboard.instantiateViewController(withIdentifier: "FilterViewController")
            as? FilterViewController {
            controller.viewModel = FilterViewModel(filters: filters, title: title)
            controller.viewModel.delegate = prevController
            pushViewController(controller, animated: true)
        }
    }

    func navigateToFilterOptions(with filter: FilterOptions, from prevController: FilterOptionsViewModelDelegate) {
        let filterStoryboard = UIStoryboard(name: "Filter", bundle: nil)
        if let controller = filterStoryboard.instantiateViewController(withIdentifier: "FilterOptionsViewController")
            as? FilterOptionsViewController {
            controller.viewModel = FilterOptionsViewModel(filter: filter)
            controller.viewModel.delegate = prevController
            pushViewController(controller, animated: true)
        }
    }

    func navigateToFilterRange(with filter: FilterRange, from prevController: FilterRangeViewModelDelegate) {
        let filterStoryboard = UIStoryboard(name: "Filter", bundle: nil)
        if let controller = filterStoryboard.instantiateViewController(withIdentifier: "FilterRangeViewController")
            as? FilterRangeViewController {
            controller.viewModel = FilterRangeViewModel(filter: filter)
            controller.viewModel.delegate = prevController
            pushViewController(controller, animated: true)
        }
    }
}
