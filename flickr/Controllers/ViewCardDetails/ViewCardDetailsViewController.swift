//
//  ViewCardDetailsViewController.swift
//  yugioh viewer
//
//  Created by David Vieth on 3/27/17.
//  Copyright © 2017 David Vieth. All rights reserved.
//

import UIKit
import CoreData
import SystemConfiguration
import Kingfisher
import Network

class ViewCardDetailsViewController: UIViewController {
    // MARK: Outlets

    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView?

    @IBOutlet weak var searchBar: UISearchBar?
    @IBOutlet weak var searchSuggestions: UITableView?
    @IBOutlet weak var searchSuggestionsHeight: NSLayoutConstraint?

    @IBOutlet weak var photoImageView: UIImageView?
    @IBOutlet weak var photoTitleLabel: UILabel?
    @IBOutlet weak var descriptionTextLabel: UILabel?

    @IBOutlet weak var detailsStackView: CardDetailsStackView!

    @IBOutlet weak var navBarView: NavBarView?

    // MARK: Properties

    var keyboardOnScreen = false
    var showDetails = true {
        didSet {
            photoTitleLabel?.alpha = showDetails ? 1 : 0
            descriptionTextLabel?.alpha = showDetails ? 1 : 0
            detailsStackView.alpha = showDetails ? 1 : 0
        }
    }

    var isSearching = false
    var searchableCards: [String] {
        viewModel.cards?.compactMap({ $0.name }) ?? Constants.Misc.commonCards
    }
    var filtered: [String] = []

    let monitor = NWPathMonitor()
    var isNetworkConnected = true

    var gradientLayer: CAGradientLayer?

    var viewModel: ViewCardDetailsViewModel! {
        didSet {
            viewModel.vcDelegate = self
        }
    }

    // MARK: Constraints

    var photoWidthConstraint: NSLayoutConstraint?
    var photoHeightConstraint: NSLayoutConstraint?

    var photoLabelTopPhotoImageConstraint: NSLayoutConstraint?
    var photoLabelTopSearchBarConstraint: NSLayoutConstraint?
    var photoLabelLeadingSuperViewConstraint: NSLayoutConstraint?
    var photoLabelLeadingStackViewConstraint: NSLayoutConstraint?

    var stackViewTrailingConstraint: NSLayoutConstraint?
    var stackViewWidthConstraint: NSLayoutConstraint?

    // MARK: UIView

    override func viewDidLoad() {
        super.viewDidLoad()

        setView()

        generateConstraints()

        navBarView?.delegate = self
        navBarView?.titleLabel.text = "View Card"
        searchSuggestions?.delegate = self
        searchSuggestions?.dataSource = self
        searchBar?.delegate = self
        searchBar?.autocorrectionType = .no

        setGestures()

        monitor.pathUpdateHandler = { [weak self] path in
            self?.isNetworkConnected = path.status == .satisfied
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        switch UIApplication.shared.statusBarOrientation {
        case .landscapeLeft, .landscapeRight:
            setLandscapeConstraints()
        case .portrait, .portraitUpsideDown, .unknown:
            setPortaitConstraints()
        @unknown default:
            setPortaitConstraints()
        }

        subscribeToNotification("UIKeyboardWillShow", selector: #selector(keyboardWillShow))
        subscribeToNotification("UIKeyboardWillHide", selector: #selector(keyboardWillHide))
        subscribeToNotification("UIKeyboardDidShow", selector: #selector(keyboardDidShow))
        subscribeToNotification("UIKeyboardDidHide", selector: #selector(keyboardDidHide))

        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unsubscribeFromAllNotifications()
        monitor.cancel()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        gradientLayer?.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: size)
        showDetails = true

        if UIDevice.current.orientation.isLandscape {
            photoImageView?.transform = CGAffineTransform(scaleX: 1, y: 1)
            setLandscapeConstraints()
        } else {
            setPortaitConstraints()
            stackViewWidthConstraint?.constant = detailsStackView.frame.width
        }
    }

    /// Set the initial display for the view
    func setView() {
        setGradient()

        searchSuggestions?.layer.zPosition = 1

        if let image = Constants.CardInfo.image {
            setImage(to: image)
        } else {
            photoImageView?.image = UIImage(named: "defaultCard")
        }

        if let cardData = Constants.CardInfo.card {
            setDetails(with: cardData)
        }
    }

    /// Set the graident for the view background
    func setGradient() {
        if gradientLayer != nil {
            gradientLayer?.removeFromSuperlayer()
            gradientLayer = nil
        }

        let layer = CAGradientLayer()
        layer.frame = view.bounds

        let gold1 = UIColor(hexCode: "#FFC900")
        let gold2 = UIColor(hexCode: "#D5AD18")
        let silver1 = UIColor(hexCode: "#EAEAEA")
        let silver2 = UIColor(hexCode: "#CCCCCC")
        layer.colors = [gold1.cgColor, gold2.cgColor, silver1.cgColor, silver2.cgColor]

        layer.locations = [0.0, 0.25, 0.75, 1.0]
        view.layer.insertSublayer(layer, at: 0)
        gradientLayer = layer
    }

    /// Create and add the gestures
    func setGestures() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(showPrev))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(showNext))
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(swipeLeft)

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self,
                                                                 action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false

        let imageTap = UITapGestureRecognizer(target: self, action: #selector(resizeImage))
        photoImageView?.addGestureRecognizer(imageTap)

        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longTap))
        photoImageView?.addGestureRecognizer(longGesture)
    }

    /// Grow or shrink the card image with animation
    @objc func resizeImage() {
        guard let photoImageView = photoImageView else {
            return
        }

        if isSearching {
            searchBar?.endEditing(true)
            dismissKeyboard()
            return
        }

        if UIDevice.current.orientation.isLandscape {
            return
        }

        let translationX = photoImageView.frame.width * 0.175
        let translationY = photoImageView.frame.height * 0.175

        UIView.animate(withDuration: 1, animations: { [weak self] in
            guard let showDetails = self?.showDetails else {
                return
            }
            photoImageView.transform = showDetails
                ? CGAffineTransform(scaleX: 1.5, y: 1.5).translatedBy(x: translationX, y: translationY)
                : CGAffineTransform(scaleX: 1, y: 1)
            self?.showDetails.toggle()
        })
    }

    /// creates alert controller with options for handling card
    @objc func longTap(_ sender: UIGestureRecognizer) {
        if sender.state == .began {
            showCardMenu()
        }
    }

    /// callback after attemtping to save image to device
    @objc func saveImageCallback(_ image: UIImage,
                                 didFinishSavingWithError error: NSError?,
                                 contextInfo: UnsafeRawPointer) {
        if let error = error {
            let alert = UIAlertController(title: "Failed to Save Image",
                                          message: error.localizedDescription,
                                          preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }

    /// make api calls for card data
    func searchCard() {
        dismissKeyboard()
        setUIEnabled(to: false)

        searchBar?.text = searchBar?.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        searchBar?.text = searchBar?.text?.replacingOccurrences(of: "#", with: "")

        if !isNetworkConnected {
            displayError("Internet Unavailable")
        } else if let searchText = searchBar?.text, !searchText.isEmpty {
            photoTitleLabel?.text = "Searching..."
            viewModel.fetchCard(with: searchText)
        } else {
            setUIEnabled(to: true)
        }
    }

    /// set UI elements to be enabled/disabled based on paramater
    func setUIEnabled(to isEnabled: Bool) {
        photoTitleLabel?.isEnabled = isEnabled
        photoImageView?.isUserInteractionEnabled = isEnabled

        activityIndicatorView?.isHidden = isEnabled
        if !isEnabled {
            activityIndicatorView?.startAnimating()
        } else {
            activityIndicatorView?.stopAnimating()
        }
    }

    func displayError(_ message: String) {
        showToast(message: message)
        DispatchQueue.main.async(execute: { [weak self] () -> Void in
            self?.setUIEnabled(to: true)
        })
    }
}
