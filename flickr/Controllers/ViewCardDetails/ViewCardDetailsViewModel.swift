//
//  ViewCardDetailsViewModel.swift
//  Yu-Gi-Oh ViewerTests
//
//  Created by David Vieth on 4/1/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import UIKit
import CoreData

protocol ViewCardDetailsViewModelDelegate: class {
    func onError(_ error: String)
    func setDetails(with card: CardData)
}

class ViewCardDetailsViewModel {
    weak var vcDelegate: ViewCardDetailsViewModelDelegate?

    var deck: Deck?
    var cards: [CardData]?
    var displayIndex: Int?

    init(deck: Deck? = nil, cards: [CardData]? = nil, displayIndex: Int? = nil) {
        self.deck = deck
        self.cards = cards
        self.displayIndex = displayIndex
    }

    /// add card to the deck
    func save(_ card: CardData, to deck: Deck) {
        guard let delegate = UIApplication.shared.delegate as? AppDelegate else {
            vcDelegate?.onError("Delegate not found")
            return
        }
        let stack = delegate.stack

        let fetchRequest = Card.createFetchRequest()

        do {
            let cards = try stack.context.fetch(fetchRequest)

            if let existingCard = cards.first(where: { $0.name == card.name }) {
                if let existingRelation = existingCard.getDeckRelation(for: deck) {
                    existingRelation.setValue((existingRelation.amount?.intValue ?? 0) + 1, forKey: "amount")
                } else {
                    _ = CardToDeck(card: existingCard, deck: deck, context: stack.context)
                }
            } else {
                let newCard = Card(card: card, context: stack.context)
                _ = CardToDeck(card: newCard, deck: deck, context: stack.context)
            }
        } catch {
            vcDelegate?.onError((error as NSError).localizedDescription)
        }
    }

    /// Fetch decks from Core Data
    func getDecks() -> [Deck]? {
        let delegate = UIApplication.shared.delegate as? AppDelegate
        let stack = delegate?.stack

        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Deck")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]

        do {
            let result = try stack?.context.fetch(fetchRequest)
            return result as? [Deck]
        } catch {
            vcDelegate?.onError((error as NSError).localizedDescription)
            return nil
        }
    }

    /// make api call for card details with current value of search bar
    func fetchCard(with text: String) {
        getCardData(for: text) { [weak self] (_ errorMessage: String?, _ card: CardDataDecodable?) in
            if let errorMessage = errorMessage {
                self?.vcDelegate?.onError(errorMessage)
            } else if let card = card?.toCardData() {
                DispatchQueue.main.async(execute: { [weak self] () -> Void in
                    self?.vcDelegate?.setDetails(with: card)

                    Constants.CardInfo.name = card.name
                    Constants.CardInfo.card = card
                })
            }
        }
    }
}
