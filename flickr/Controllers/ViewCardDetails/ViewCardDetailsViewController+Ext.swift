//
//  ViewCardDetailsViewController+Ext.swift
//  yugiohViewer
//
//  Created by David Vieth on 3/22/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import UIKit

// MARK: ViewCardDetailsViewModelDelegate - Notifications

extension ViewCardDetailsViewController {
    func subscribeToNotification(_ notification: String, selector: Selector) {
        NotificationCenter.default.addObserver(self,
                                               selector: selector,
                                               name: NSNotification.Name(rawValue: notification),
                                               object: nil)
    }

    func unsubscribeFromAllNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: ViewCardDetailsViewModelDelegate - Keyboard Helper Functions

extension ViewCardDetailsViewController {
    /// removes keyboard from screen
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    /// callback for keyboard animating on to screen
    @objc func keyboardWillShow(_ notification: Notification) {
        if !keyboardOnScreen {
            view.frame.origin.y -= getKeyboardHeight(from: notification)
        }
    }

    /// callback for keyboard animating off of screen
    @objc func keyboardWillHide(_ notification: Notification) {
        if keyboardOnScreen {
            view.frame.origin.y += getKeyboardHeight(from: notification)
        }
    }

    /// callback for keyboard finished animating on to screen
    @objc func keyboardDidShow(_ notification: Notification) {
        keyboardOnScreen = true
    }

    /// callback for keyboard finished animating off of screen
    @objc func keyboardDidHide(_ notification: Notification) {
        keyboardOnScreen = false
    }

    /// get keyboard height from notification
    func getKeyboardHeight(from notification: Notification) -> CGFloat {
        if let userInfo = (notification as Notification).userInfo,
           let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            return keyboardSize.cgRectValue.height
        } else {
            return 0
        }
    }
}

// MARK: - ViewCardDetailsViewController - Set Display Helpers

extension ViewCardDetailsViewController {
    /// display next card in deck
    @objc func showNext() {
        if let cards = viewModel.cards, let index = viewModel.displayIndex {
            if index != (cards.count) - 1 {
                viewModel.displayIndex = index + 1
                setCardDisplay(to: cards[index + 1], style: "right")
            } else {
                viewModel.displayIndex = 0
                setCardDisplay(to: cards[0], style: "right")
            }
        }
    }

    /// display previous card in deck
    @objc func showPrev() {
        if let cards = viewModel.cards, let index = viewModel.displayIndex {
            if index != 0 {
                viewModel.displayIndex = index - 1
                setCardDisplay(to: cards[index - 1], style: "left")
            } else {
                guard let last = cards.last else {
                    return
                }
                viewModel.displayIndex = cards.count - 1
                setCardDisplay(to: last, style: "left")
            }
        }
    }

    /// set details and image for card with style setting animation
    func setCardDisplay(to card: CardData, style: String) {
        setImage(to: APIRouter.getImage(name: card.name).url, style: style)
        setDetails(with: card)
    }

    /// set the card image
    func setImage(to url: URL?, style: String) {
        if style == "left" {
            animateImageViewSlide(url: url, next: false)
        } else if style == "right" {
            animateImageViewSlide(url: url, next: true)
        } else {
            setImage(to: url)
        }
    }

    func setImage(to url: URL?) {
        photoImageView?.kf.setImage(with: url, placeholder: UIImage(named: "defaultCard")) { [weak self] result in
            switch result {
            case .success(let value):
                Constants.CardInfo.image = url
                print("Successfully set image to \(value.source.url?.absoluteString ?? "n/a")")
            case .failure(let error):
                print("Failed to set image with error: \(error.localizedDescription)")
                self?.displayError(error.localizedDescription)
            }
        }
    }

    /// animate image sliding on to screen
    func animateImageViewSlide(url: URL?, next: Bool) {
        CATransaction.begin()

        CATransaction.setAnimationDuration(0.25)

        let transition = CATransition()
        transition.type = CATransitionType.push
        if next {
            transition.subtype = CATransitionSubtype.fromRight
        } else {
            transition.subtype = CATransitionSubtype.fromLeft
        }

        photoImageView?.layer.add(transition, forKey: kCATransition)
        setImage(to: url)

        descriptionTextLabel?.layer.add(transition, forKey: kCATransition)
        photoTitleLabel?.layer.add(transition, forKey: kCATransition)
        detailsStackView?.layer.add(transition, forKey: kCATransition)

        CATransaction.commit()
    }
}

// MARK: ViewCardDetailsViewController - Show Menu

extension ViewCardDetailsViewController {
    /// create alert controller with options for handling card on screen
    func showCardMenu() {
        guard let photoImageView = photoImageView, let image = photoImageView.image else {
            return
        }

        let optionMenu = UIAlertController(title: nil,
                                           message: "Choose Option",
                                           preferredStyle: .actionSheet)

        var saveAction: UIAlertAction
        if let deck = viewModel.deck {
            saveAction = UIAlertAction(title: "Add to Deck", style: .default) { [weak self] _ -> Void in
                self?.viewModel.save(Constants.CardInfo.card, to: deck)
            }
        } else {
            saveAction = UIAlertAction(title: "Add to Deck", style: .default) { [weak self] _ -> Void in
                self?.showDecksMenu()
            }
        }

        let downloadAction = UIAlertAction(title: "Save Image", style: .default) { [weak self] _ -> Void in
            UIImageWriteToSavedPhotosAlbum(image,
                                           self,
                                           #selector(
                                            self?.saveImageCallback(
                                                _:didFinishSavingWithError:contextInfo:
                                            )),
                                           nil)
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ -> Void in
            print("Cancelled")
        }

        optionMenu.addAction(saveAction)
        optionMenu.addAction(downloadAction)
        optionMenu.addAction(cancelAction)

        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
            optionMenu.modalPresentationStyle = UIModalPresentationStyle.popover
            let popPresenter = optionMenu.popoverPresentationController!
            popPresenter.sourceView = photoImageView
            popPresenter.sourceRect = photoImageView.bounds
        }

        present(optionMenu, animated: true, completion: nil)
    }

    /// create alert controller with options for selecting an existing deck
    func showDecksMenu() {
        guard let photoImageView = photoImageView else {
            return
        }

        let optionMenu = UIAlertController(title: nil, message: "Choose Deck", preferredStyle: .actionSheet)

        // Get Decks and Add Actions
        if let decks = viewModel.getDecks() {
            for element in decks {
                optionMenu.addAction(UIAlertAction(title: element.name, style: .default) { [weak self] _ -> Void in
                    self?.viewModel.save(Constants.CardInfo.card, to: element)
                })
            }
        } else {
            return
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {_ -> Void in
            print("Cancelled")
        }

        optionMenu.addAction(cancelAction)

        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
            optionMenu.modalPresentationStyle = UIModalPresentationStyle.popover
            guard let popPresenter = optionMenu.popoverPresentationController else {
                return
            }
            popPresenter.sourceView = photoImageView
            popPresenter.sourceRect = photoImageView.bounds
        }

        present(optionMenu, animated: true, completion: nil)
    }
}

// MARK: ViewCardDetailsViewController - Constraints

extension ViewCardDetailsViewController {
    func generateConstraints() {
        if let photoImageView = photoImageView,
           let photoTitleLabel = photoTitleLabel,
           let detailsStackView = detailsStackView,
           let searchBar = searchBar {
            photoWidthConstraint = photoImageView.widthAnchor.constraint(equalTo: view.widthAnchor,
                                                                         multiplier: 0.667,
                                                                         constant: -32)
            photoHeightConstraint = photoImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -16)
            photoLabelTopPhotoImageConstraint = photoImageView.bottomAnchor.constraint(
                equalTo: photoTitleLabel.topAnchor,
                constant: -8
            )

            photoLabelTopSearchBarConstraint = searchBar.bottomAnchor.constraint(equalTo: photoTitleLabel.topAnchor,
                                                                          constant: -8)
            photoLabelLeadingSuperViewConstraint = photoTitleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor,
                                                                                            constant: 24)
            photoLabelLeadingStackViewConstraint = photoTitleLabel.leadingAnchor.constraint(
                equalTo: detailsStackView.trailingAnchor,
                constant: 8
            )

            stackViewTrailingConstraint = detailsStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor,
                                                                              constant: -24)
            stackViewWidthConstraint = detailsStackView.widthAnchor.constraint(equalToConstant: 120)
        }
    }

    func setPortaitConstraints() {
        NSLayoutConstraint.deactivate([
            photoHeightConstraint,
            photoLabelLeadingStackViewConstraint,
            photoLabelTopSearchBarConstraint,
            stackViewWidthConstraint
        ].compactMap({ $0 }))
        NSLayoutConstraint.activate([
            photoLabelLeadingSuperViewConstraint,
            photoLabelTopPhotoImageConstraint,
            photoWidthConstraint,
            stackViewTrailingConstraint
        ].compactMap({ $0 }))
    }

    func setLandscapeConstraints() {
        NSLayoutConstraint.deactivate([
            photoLabelLeadingSuperViewConstraint,
            photoLabelTopPhotoImageConstraint,
            photoWidthConstraint,
            stackViewTrailingConstraint
        ].compactMap({ $0 }))
        NSLayoutConstraint.activate([
            photoHeightConstraint,
            photoLabelLeadingStackViewConstraint,
            photoLabelTopSearchBarConstraint,
            stackViewWidthConstraint
        ].compactMap({ $0 }))
    }
}
