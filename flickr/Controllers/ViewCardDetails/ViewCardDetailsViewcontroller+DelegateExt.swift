//
//  ViewCardDetailsViewcontroller+DelegateExt.swift
//  yugiohViewer
//
//  Created by David Vieth on 4/6/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import UIKit

// MARK: - UISearchBarDelegate

extension ViewCardDetailsViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearching = true
        animateHeight(to: CGFloat(min(25 * filtered.count, 100)))

        searchSuggestions?.reloadData()
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        isSearching = false
        let time = DispatchTime.now() + Double(Int64(250000000)) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: time) { [weak self] in
            self?.animateHeight(to: 0)
        }
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
        animateHeight(to: 0)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard photoTitleLabel?.isEnabled ?? false else {
            return
        }

        isSearching = false
        if let cards = viewModel.cards,
           cards.count != 0,
           let text = searchBar.text,
           let index = searchableCards.firstIndex(where: { $0.lowercased() == text.lowercased() }) {
            viewModel.displayIndex = index
            setCardDisplay(to: cards[index], style: "fade")
        } else {
            searchCard()
        }

        animateHeight(to: 0)
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            isSearching = false
            filtered.removeAll()
            animateHeight(to: 0)
        } else {
            filtered = searchableCards.filter({ (text) -> Bool in
                let tmp: NSString = text as NSString
                let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                return range.location != NSNotFound
            })
            if filtered.count > 25 {
                filtered = Array(filtered.prefix(25))
            }
            filtered.sort()
            filtered.insert(searchText, at: 0)

            isSearching = true
            animateHeight(to: CGFloat(min(25 * filtered.count, 100)))
        }
        searchSuggestions?.reloadData()
    }

    /// change the height of the search suggestions view with animation
    func animateHeight(to height: CGFloat) {
        searchSuggestionsHeight?.constant = height
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource

extension ViewCardDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearching ? filtered.count : 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")! as UITableViewCell
        if isSearching {
            cell.textLabel?.text = filtered[indexPath.row]
        }

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchBar?.text = filtered[indexPath.row]
        guard let cell = tableView.cellForRow(at: indexPath) else {
            return
        }

        cell.contentView.backgroundColor = UIColor(hexCode: "#FFC900")

        if let cards = viewModel.cards,
           cards.count != 0,
           let text = searchBar?.text,
           let newIndex = searchableCards.firstIndex(of: text) {
            viewModel.displayIndex = newIndex
            setCardDisplay(to: cards[newIndex], style: "fade")
        } else {
            searchCard()
        }
    }
}

// MARK: - UITableViewDelegate, NavBarViewDelegate

extension ViewCardDetailsViewController: NavBarViewDelegate {
    func didClickBackButton() {
        navigationController?.popViewController(animated: true)
    }
}

// MARK: ViewCardDetailsViewModelDelegate

extension ViewCardDetailsViewController: ViewCardDetailsViewModelDelegate {
    func onError(_ error: String) {
        print("Error: \(error)")
        displayError(error)
    }

    func setDetails(with card: CardData) {
        photoTitleLabel?.text = card.name
        descriptionTextLabel?.text = card.text

        detailsStackView.setDetails(for: card)

        Constants.CardInfo.name = card.name
        Constants.CardInfo.card = card

        setImage(to: APIRouter.getImage(name: card.name).url)

        setUIEnabled(to: true)
    }
}
