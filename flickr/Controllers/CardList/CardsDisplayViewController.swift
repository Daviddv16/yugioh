//
//  CardDisplayViewController.swift
//  yugiohViewer
//
//  Created by David Vieth on 4/5/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import UIKit

protocol CardDisplayDelegate: class {
    var filterTile: String { get }

    var removeCardMessage: String { get }

    /// Delete list item from device, must be implemented by subclass
    func deleteItem(_ index: Int)

    /// Handler for plus button
    func pressPlusButton()

    /// Reload any data and update any views
    func reloadData()
}

class CardsDisplayViewController: UIViewController {

    // MARK: Properties

    var showDetails = true

    var viewModel: CardsDisplayViewModel!
    weak var delegate: CardDisplayDelegate?

    // MARK: UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.delegate = self

        let searchFrame = CGRect(x: 0, y: 86, width: view.frame.width - 44, height: 44)
        let searchBar = UISearchBar(frame: searchFrame)
        searchBar.searchBarStyle = .minimal
        searchBar.isTranslucent = false
        searchBar.backgroundColor = .white
        searchBar.delegate = self

        let filterFrame = CGRect(x: searchFrame.width, y: 86, width: 44, height: 44)
        let filterButton = UIButton(frame: filterFrame)
        filterButton.backgroundColor = .white
        filterButton.setFontAwesomeIcon(to: .filter)
        filterButton.setTitleColor(.systemGray, for: .normal)
        filterButton.addTarget(self, action: #selector(openFilter), for: .touchUpInside)

        view.addSubview(searchBar)
        view.addSubview(filterButton)
    }

    @objc func openFilter() {
        (navigationController as? NavController)?.navigateToFilter(with: viewModel.filters,
                                                                   delegate?.filterTile ?? "Filter Cards",
                                                                   from: self)
    }

    // MARK: Action Sheet Actions

    func displayCardDecks(at index: Int, _ completion: ((Bool) -> Void)? = nil) {
        let card = viewModel.cardArray[index]
        let alert = UIAlertController(title: card.name,
                                      message: card.getDeckList(),
                                      preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        present(alert, animated: true) {
            completion?(true)
        }
    }

    func confirmRemoveCard(at index: Int, _ completion: ((Bool) -> Void)? = nil) {
        let deleteAlert = UIAlertController(title: "Remove Card?",
                                            message: delegate?.removeCardMessage,
                                            preferredStyle: UIAlertController.Style.alert)

        deleteAlert.addAction(UIAlertAction(title: "Ok", style: .default) { [weak self] _ in
            self?.delegate?.deleteItem(index)
            completion?(true)
        })

        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in
            completion?(false)
        })

        present(deleteAlert, animated: true, completion: nil)
    }

    func getActionSheetActions(for index: Int) -> [UIAlertAction] {
        return [
            UIAlertAction(title: "Check Decks", style: .default) { [weak self] _  -> Void in
                self?.displayCardDecks(at: index)
            },
            UIAlertAction(title: "Remove Card", style: .default) { [weak self] _ -> Void in
                self?.confirmRemoveCard(at: index)
            },
            UIAlertAction(title: "Cancel", style: .cancel) { _ -> Void in
                print("Cancelled")
            }
        ]
    }
}

// MARK: FilterViewModelDelegate

extension CardsDisplayViewController: FilterViewModelDelegate {
    func onApply(filters: [Filter]) {
        viewModel.filters = filters
    }
}

// MARK: UISearchBarDelegate

extension CardsDisplayViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.filterText = searchText.lowercased()
    }
}

// MARK: CardsDisplayViewModelDelegate

extension CardsDisplayViewController: CardsDisplayViewModelDelegate {
    func didApplyFilters() {
        delegate?.reloadData()
    }
}
