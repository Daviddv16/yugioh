//
//  CardGridViewController.swift
//  yugiohViewer
//
//  Created by David Vieth on 5/6/17.
//  Copyright © 2017 David Vieth. All rights reserved.
//

import UIKit
import CoreData

class CardGridViewController: CardsDisplayViewController {

    // MARK: Outlets

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var navBarView: NavBarView!

    // MARK: UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.delegate = self
        collectionView.dataSource = self
        navBarView.delegate = self
        navBarView.titleLabel.text = "Card Grid"
        navBarView.showPlusButton()

        setLayout(with: view.frame.size)

        let longpress = UILongPressGestureRecognizer(
            target: self,
            action: #selector(longPressGestureRecognized(_:))
        )
        collectionView?.addGestureRecognizer(longpress)

        self.edgesForExtendedLayout = UIRectEdge()
        self.extendedLayoutIncludesOpaqueBars = false
        collectionView.contentInsetAdjustmentBehavior = .never
        collectionView.register(UINib(nibName: "CardCollectionViewCell", bundle: nil),
                                forCellWithReuseIdentifier: "CardCollectionViewCell")

        delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.tabBarController?.tabBar.isHidden = false

        viewModel.cardArray = []
        let arr = getAllCards()
        for item in arr where !viewModel.cardArray.contains(where: { $0.name == item.name }) {
            viewModel.cardArray.append(item)
        }

        viewModel.cardArray.sort(by: sortCards)

        collectionView.reloadData()
    }

    func setLayout(with size: CGSize) {
        let noCol: CGFloat = 3.0
        let colMargin: CGFloat = UIDevice.current.orientation.isLandscape ? 48.0 : 16.0
        let colSpacing: CGFloat = 32.0

        let flowLayout = UICollectionViewFlowLayout()
        let width = (size.width - colSpacing - ((noCol - 1) * colMargin)) / noCol
        flowLayout.minimumInteritemSpacing = colMargin
        flowLayout.minimumLineSpacing = colMargin
        flowLayout.itemSize = CGSize(width: width, height: width * 86 / 59)
        flowLayout.sectionInset = UIEdgeInsets(top: 16 + 44, left: colSpacing / 2, bottom: 0, right: colSpacing / 2)

        collectionView?.collectionViewLayout = flowLayout
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        setLayout(with: size)
    }

    /// Creates alert controller with options for handling card/grid
    @objc func longPressGestureRecognized(_ sender: UIGestureRecognizer) {
        guard let longPress = sender as? UILongPressGestureRecognizer else {
            return
        }
        let index = longPress.location(in: collectionView)
        guard let indexPath = collectionView?.indexPathForItem(at: index) else {
            return
        }

        if sender.state == .began {
            displayActionSheet(for: indexPath)
        }
    }

    func displayActionSheet(for indexPath: IndexPath) {
        let card = viewModel.filteredCards[indexPath.row]

        let optionMenu = UIAlertController(title: card.name, message: nil, preferredStyle: .actionSheet)

        for action in getActionSheetActions(for: indexPath.row) {
            optionMenu.addAction(action)
        }

        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
            let cell = collectionView(collectionView!, cellForItemAt: indexPath)

            optionMenu.modalPresentationStyle = UIModalPresentationStyle.popover
            guard let popPresenter = optionMenu.popoverPresentationController else {
                return
            }
            popPresenter.sourceView = collectionView
            popPresenter.sourceRect = cell.frame
        }

        present(optionMenu, animated: true, completion: nil)
    }
}

// MARK: UICollectionViewDelegate

extension CardGridViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.filteredCards.count
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: "CardCollectionViewCell",
                for: indexPath) as? CardCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.contentView.frame = cell.bounds

        cell.format(for: viewModel.filteredCards[indexPath.row])

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let card = viewModel.filteredCards[indexPath.row]

        Constants.CardInfo.image = APIRouter.getImage(name: card.name).url
        Constants.CardInfo.name = card.name
        Constants.CardInfo.card = card

        (navigationController as? NavController)?.navigateToViewCardDetails(cards: viewModel.filteredCards,
                                                                            displayIndex: indexPath.row)
    }
}

// MARK: NavBarViewDelegate

extension CardGridViewController: NavBarViewDelegate {
    func didClickBackButton() {
        navigationController?.popViewController(animated: true)
    }

    func didClickPlusButton() {
        delegate?.pressPlusButton()
    }
}

// MARK: CardDisplayDelegate

extension CardGridViewController: CardDisplayDelegate {
    var filterTile: String {
        "Filter All Cards"
    }

    var removeCardMessage: String {
        "Warning, this will remove the card from all decks."
    }

    func pressPlusButton() {
        (navigationController as? NavController)?.navigateToViewCardDetails()
    }

    func reloadData() {
        collectionView.reloadData()
    }

    func deleteItem(_ index: Int) {
        guard let card = viewModel.filteredCards[index].getCoreDataCard() else {
            return
        }

        let delegate = UIApplication.shared.delegate as? AppDelegate
        delegate?.stack.context.delete(card)

        viewModel.filteredCards.remove(at: index)
        collectionView!.reloadData()
    }
}
