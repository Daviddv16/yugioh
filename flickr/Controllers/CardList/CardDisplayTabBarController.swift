//
//  CardDisplayTabBarController.swift
//  yugiohViewer
//
//  Created by David Vieth on 4/19/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import UIKit
import FontAwesome

class CardDisplayTabBarController: UITabBarController {

    private var tabIcons = [FontAwesome.listUl, FontAwesome.gripHorizontal]

    override func viewDidLoad() {
        super.viewDidLoad()

        if let tabBarItems = tabBar.items {
            for index in 0..<tabBarItems.count {
                tabBarItems[index].image = UIImage.fontAwesomeIcon(name: tabIcons[index],
                                                                   style: .solid,
                                                                   textColor: .blue,
                                                                   size: CGSize(width: 30, height: 30))
            }
        }
    }
}
