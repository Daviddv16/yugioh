//
//  CardsDisplayViewModel.swift
//  yugiohViewer
//
//  Created by David Vieth on 4/8/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import UIKit
import CoreData

enum CardFilter: String {
    case cardType = "Card Type"
    case type = "Type"
    case level = "Level"
    case attribute = "Attribute"
    case atk = "Attack Points"
    case def = "Defense Points"
    case property = "Property"

    var isMonsterOnly: Bool {
        switch self {
        case .type, .level, .attribute, .atk, .def:
            return true
        case .cardType, .property:
            return false
        }
    }

    var isSpellTrapOnly: Bool {
        switch self {
        case .property:
            return true
        case .cardType, .type, .level, .attribute, .atk, .def:
            return false
        }
    }

    var defaultFilter: Filter {
        switch self {
        case .cardType:
            return FilterOptions(displayName: rawValue,
                         options: [
                          ("monster", false),
                          ("spell", false),
                          ("trap", false)
                         ],
                         type: .multiSelect)
        case .type:
            return FilterOptions(displayName: rawValue,
                         options: [
                            ("Effect", false),
                            ("Fusion", false),
                            ("Xyz", false),
                            ("Synchro", false),
                            ("Ritual", false)
                           ],
                         type: .multiSelect)
        case .level:
            return FilterOptions(displayName: rawValue,
                         options: [
                            ("1", false),
                            ("2", false),
                            ("3", false),
                            ("4", false),
                            ("5", false),
                            ("6", false),
                            ("7", false),
                            ("8", false),
                            ("9", false),
                            ("10", false),
                            ("11", false),
                            ("12", false)
                           ],
                         type: .multiSelect)
        case .attribute:
            return FilterOptions(displayName: rawValue,
                         options: [
                            ("wind", false),
                            ("earth", false),
                            ("fire", false),
                            ("water", false),
                            ("light", false),
                            ("dark", false)
                           ],
                         type: .multiSelect)
        case .atk, .def:
            return FilterRange(displayName: rawValue)
        case .property:
            return FilterOptions(displayName: rawValue,
                                 options: SpellTrapCard.Properties.allCases.map({ ($0.rawValue, false) }),
                                 type: .multiSelect)
        }
    }

    func canSkip(card: CardData) -> Bool {
        if isMonsterOnly {
            return card.cardType != "monster"
        }

        if isSpellTrapOnly {
            return card.cardType != "spell" && card.cardType != "trap"
        }

        return false
    }
}

protocol CardsDisplayViewModelDelegate: class {
    func didApplyFilters()
}

class CardsDisplayViewModel {

    // MARK: Properties

    var cardArray: [CardData] = [] {
        didSet {
            applyFilters()
        }
    }
    var sortMethodFilter: FilterOptions {
        let currentValue = Config.getSortMethodProperty()
        let options = Config.SortMethods.allCases.map({ ($0.displayValue, currentValue == $0 )})
        return FilterOptions(displayName: "Sort Method",
                             options: options,
                             defaultSelectedIndex: 0,
                             type: .singleSelectRequired)
    }

    var filters: [Filter] = [] {
        didSet {
            applyFilters()
        }
    }
    var filterText = "" {
        didSet {
            applyFilters()
        }
    }
    var filteredCards: [CardData] = []

    weak var delegate: CardsDisplayViewModelDelegate?

    init() {
        filters = [
            CardFilter.cardType.defaultFilter,
            CardFilter.type.defaultFilter,
            CardFilter.level.defaultFilter,
            CardFilter.attribute.defaultFilter,
            CardFilter.atk.defaultFilter,
            CardFilter.def.defaultFilter,
            CardFilter.property.defaultFilter,
            sortMethodFilter,
            FilterOptions(displayName: "Sort Direction",
                   options: [
                    ("Ascending", Config.getSortDirectionProperty()),
                    ("Descending", !Config.getSortDirectionProperty())
                   ],
                   defaultSelectedIndex: 0,
                   type: .singleSelectRequired)
        ]
    }

    // MARK: CardsDisplayViewModel

    func applyFilters() {
        var cards = cardArray

        if filterText != "" {
            cards = cards.filter({ $0.name.lowercased().contains(filterText) })
        }

        for filter in filters {
            if let rangeFilter = filter as? FilterRange {
                if rangeFilter.isEnabled {
                    cards = cards.filter({ filterCardByRange(card: $0, filter: rangeFilter) })
                }

                continue
            } else if let optionsFilter = filter as? FilterOptions {
                cards = handleFilterOptions(optionsFilter, for: cards)
            }
        }

        cards.sort(by: sortCards)
        filteredCards = cards
        delegate?.didApplyFilters()
    }

    func filterCardByRange(card: CardData, filter: FilterRange) -> Bool {
        guard let cardFilter = CardFilter(rawValue: filter.displayName) else {
            return true
        }

        var value: Int?
        if cardFilter == .atk {
            value = (card as? MonsterCard)?.atk
        } else if cardFilter == .def {
            value = (card as? MonsterCard)?.def
        }

        guard let compareValue = value, let minimum = filter.minimum, let maximum = filter.maximum else {
            return true
        }

        return cardFilter.canSkip(card: card) || (compareValue >= minimum && compareValue <= maximum)
    }

    func handleFilterOptions(_ filter: FilterOptions, for cards: [CardData]) -> [CardData] {
        guard !handleViewFilter(filter) else {
            return cards
        }

        let filterOptions = filter.options.filter({ $0.1 }).map({ $0.0 })
        guard filterOptions.count > 0 else {
            return cards
        }

        guard let cardFilter = CardFilter(rawValue: filter.displayName) else {
            return cards
        }

        return handleCardFilter(cardFilter, with: filterOptions, for: cards)
    }

    func handleViewFilter(_ filter: FilterOptions) -> Bool {
        guard filter.displayName != "Sort Direction" else {
            if let index = filter.options.firstIndex(where: { $0.1 }) {
                Config.setSortDirectionProperty(to: index == 0)
            }
            return true
        }

        guard filter.displayName != "Sort Method" else {
            if let index = filter.options.firstIndex(where: { $0.1 }) {
                Config.setSortMethodProperty(to: Config.SortMethods.allCases[index])
            }
            return true
        }

        return false
    }

    func handleCardFilter(_ filter: CardFilter, with options: [String], for cards: [CardData]) -> [CardData] {
        switch filter {
        case .cardType:
            return cards.filter({ filter.canSkip(card: $0) ||
                                    options.contains($0.cardType) })
        case .type:
            return cards.filter({ (card) in
                                    filter.canSkip(card: card) ||
                                        options.contains(
                                            where: { (card as? MonsterCard)?.type.contains($0) ?? false }
                                        ) })
        case .level:
            return cards.filter({ (card) in filter.canSkip(card: card) ||
                                    options.map({ Int($0) }).contains((card as? MonsterCard)?.level) })
        case .attribute:
            return cards.filter({ filter.canSkip(card: $0) ||
                                    options.contains(($0 as? MonsterCard)?.attribute ?? "") })
        case .atk, .def:
            return cards
        case .property:
            return cards.filter({ filter.canSkip(card: $0) ||
                                    options.contains(($0 as? SpellTrapCard)?.property.rawValue ?? "") })
        }
    }
}
