//
//  CardTableViewController.swift
//  yugiohViewer
//
//  Created by David Vieth on 5/2/17.
//  Copyright © 2017 David Vieth. All rights reserved.
//

import UIKit
import CoreData

class CardTableViewController: CardsDisplayViewController {

    // MARK: Outlets

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navBarView: NavBarView!

    // MARK: UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.filters.append(
            FilterOptions(displayName: "View Details",
                   options: [("", Config.getShowDetailsProperty())],
                   defaultSelectedIndex: 0,
                   type: .boolean)
            )

        tableView.delegate = self
        tableView.dataSource = self
        navBarView.delegate = self
        navBarView.showPlusButton()

        let longpress = UILongPressGestureRecognizer(
            target: self,
            action: #selector(longPressGestureRecognized(_:))
        )
        tableView.addGestureRecognizer(longpress)
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.register(UINib(nibName: "CardTableViewCell", bundle: nil),
                           forCellReuseIdentifier: "CardTableViewCell")

        edgesForExtendedLayout = UIRectEdge()
        extendedLayoutIncludesOpaqueBars = false
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        showDetails = Config.getShowDetailsProperty()
    }

    /// Creates alert controller with options for handling card/table
    @objc func longPressGestureRecognized(_ sender: UIGestureRecognizer) {
        guard let gesturePoint = (sender as? UILongPressGestureRecognizer)?.location(in: tableView),
              let indexPath = tableView.indexPathForRow(at: gesturePoint) else {
            return
        }
        let index = indexPath.row - 1

        if sender.state == .began {
            let name = viewModel.filteredCards[index].name

            let optionMenu = UIAlertController(title: name, message: nil, preferredStyle: .actionSheet)

            for action in getActionSheetActions(for: index) {
                optionMenu.addAction(action)
            }

            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
                let cell = tableView(tableView, cellForRowAt: indexPath)

                optionMenu.modalPresentationStyle = UIModalPresentationStyle.popover
                guard let popPresenter = optionMenu.popoverPresentationController else {
                    return
                }
                popPresenter.sourceView = tableView
                popPresenter.sourceRect = cell.frame
            }

            self.present(optionMenu, animated: true, completion: nil)
        }
    }

    // MARK: CardDisplayDelegate

    func reloadData() {
        tableView.reloadData()
    }
}

// MARK: UITableViewDelegate, UITableViewDataSource

extension CardTableViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row != 0 else {
            return 44
        }

        return 100
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.filteredCards.count + 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row != 0 else {
            return UITableViewCell()
        }

        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CardTableViewCell", for: indexPath)
                as? CardTableViewCell else {
            return UITableViewCell()
        }

        let index = indexPath.row - 1

        cell.format(for: viewModel.filteredCards[index], withDetails: showDetails)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row != 0 else {
            tableView.deselectRow(at: indexPath, animated: true)
            return
        }
        let index = indexPath.row - 1

        let card = viewModel.filteredCards[index]

        Constants.CardInfo.image = APIRouter.getImage(name: card.name).url
        Constants.CardInfo.name = card.name
        Constants.CardInfo.card = card

        (navigationController as? NavController)?.navigateToViewCardDetails(cards: viewModel.filteredCards,
                                                                            displayIndex: index)
    }

    func tableView(_ tableView: UITableView,
                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        return UISwipeActionsConfiguration(
            actions: [
                UIContextualAction(style: .destructive, title: "Delete") { [weak self] (_, _, completion) in
                    self?.confirmRemoveCard(at: indexPath.row - 1, completion)
                },
                UIContextualAction(style: .normal, title: "Check Decks") { [weak self] (_, _, completion) in
                    self?.displayCardDecks(at: indexPath.row - 1, completion)
                }
            ]
        )
    }
}

// MARK: NavBarViewDelegate

extension CardTableViewController: NavBarViewDelegate {
    func didClickBackButton() {
        navigationController?.popViewController(animated: true)
    }

    func didClickPlusButton() {
        delegate?.pressPlusButton()
    }
}
