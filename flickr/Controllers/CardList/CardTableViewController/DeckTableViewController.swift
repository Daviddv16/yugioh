//
//  DeckTableViewController.swift
//  Yu-Gi-Oh Card Finder
//
//  Created by David Vieth on 3/21/17.
//  Copyright © 2017 David Vieth. All rights reserved.
//

import UIKit
import CoreData

class DeckTableViewController: CardTableViewController {

    // MARK: Properties

    var deckObject: Deck? {
        didSet {
            getCardsFromDeck()
            navBarView.titleLabel.text = deckObject?.name ?? "Deck"
        }
    }

    // MARK: CardTableViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        let delegate = UIApplication.shared.delegate as? AppDelegate
        let stack = delegate?.stack

        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Deck")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]

        do {
            guard let result = try stack?.context.fetch(fetchRequest) as? [Deck],
                  let index = Constants.Misc.deckIndex else {
                print("Error getting decks")
                return
            }

            deckObject = result[index]
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }

        tableView.reloadData()
    }

    func getCardsFromDeck() {
        guard let deck = deckObject else {
            return
        }

        viewModel.cardArray = deck.getCards()
        viewModel.cardArray.sort(by: sortCards)
    }
}

// MARK: CardDisplayDelegate

extension DeckTableViewController: CardDisplayDelegate {
    var filterTile: String {
        "Filter Deck Cards"
    }

    var removeCardMessage: String {
        "Are you sure you want to remove this card?"
    }

    func deleteItem(_ index: Int) {
        let delegate = UIApplication.shared.delegate as? AppDelegate
        let stack = delegate?.stack

        guard let card = viewModel.filteredCards[index].getCoreDataCard() else {
            return
        }

        guard let relation = deckObject?.getCardRelation(for: card) else {
            return
        }

        if let amount = relation.amount?.intValue, amount > 1 {
            relation.setValue(amount - 1, forKey: "amount")
        } else if card.decks.count > 1 {
            stack?.context.delete(relation)
        } else {
            stack?.context.delete(card)
        }

        viewModel.filteredCards.remove(at: index)
        tableView.reloadData()
    }

    func pressPlusButton() {
        (navigationController as? NavController)?.navigateToViewCardDetails(deck: deckObject)
    }
}
