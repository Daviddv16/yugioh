//
//  CardTableViewModel.swift
//  yugiohViewer
//
//  Created by David Vieth on 4/12/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import Foundation

class CardTableViewModel: CardsDisplayViewModel {
    override func handleViewFilter(_ filter: FilterOptions) -> Bool {
        guard filter.displayName != "View Details" else {
            if let value = filter.options.first?.1 {
                Config.setShowDetailsProperty(to: value)
            }
            return true
        }

        return super.handleViewFilter(filter)
    }
}
