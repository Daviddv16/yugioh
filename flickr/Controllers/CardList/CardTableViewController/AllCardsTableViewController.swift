//
//  AllCardsTableViewController.swift
//  Yu-Gi-Oh Card Finder
//
//  Created by David Vieth on 3/21/17.
//  Copyright © 2017 David Vieth. All rights reserved.
//

import UIKit
import CoreData

class AllCardsTableViewController: CardTableViewController {

    // MARK: CardTableViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        delegate = self
        navBarView.titleLabel.text = "Card List"
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        viewModel.cardArray = getAllCards()
        viewModel.cardArray.sort(by: sortCards)

        tableView.reloadData()
    }
}

// MARK: CardDisplayDelegate

extension AllCardsTableViewController: CardDisplayDelegate {
    var filterTile: String {
        "Filter All Cards"
    }

    var removeCardMessage: String {
        "Warning, this will remove the card from all decks."
    }

    func deleteItem(_ index: Int) {
        guard let card = viewModel.filteredCards[index].getCoreDataCard() else {
            return
        }

        let delegate = UIApplication.shared.delegate as? AppDelegate
        delegate?.stack.context.delete(card)

        viewModel.filteredCards.remove(at: index)
        tableView.reloadData()
    }

    func pressPlusButton() {
        (navigationController as? NavController)?.navigateToViewCardDetails()
    }
}
