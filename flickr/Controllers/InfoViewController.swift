//
//  InfoViewController.swift
//  yugiohViewer
//
//  Created by David Vieth on 1/31/18.
//  Copyright © 2018 David Vieth. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {
    // MARK: Outlets

    @IBOutlet weak var navBarView: NavBarView!

    // MARK: UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        navBarView.delegate = self
        navBarView.titleLabel.text = "Info"
    }
}

// MARK: NavBarViewDelegate

extension InfoViewController: NavBarViewDelegate {
    func didClickBackButton() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
}
