//
//  Deck.swift
//  yugiohViewer
//
//  Created by David Vieth on 4/27/17.
//  Copyright © 2017 David Vieth. All rights reserved.
//

import Foundation
import CoreData

class Deck: NSManagedObject {

    var amount: Int {
        let cardRelations = (cards.allObjects as? [CardToDeck]) ?? []
        return cardRelations.reduce(0, { $0 + ($1.amount?.intValue ?? 0) })
    }

    convenience init (name: String, context: NSManagedObjectContext) {
        if let ent = NSEntityDescription.entity(forEntityName: "Deck", in: context) {
            self.init(entity: ent, insertInto: context)
            self.name = name
        } else {
            fatalError("Unable to find Entity name: Deck")
        }
    }

    func getCardRelation(for card: Card) -> CardToDeck? {
        return (cards.allObjects as? [CardToDeck])?.first(where: { $0.card?.name == card.name })
    }

    func getCards() -> [CardData] {
        let cardRelations = (cards.allObjects as? [CardToDeck]) ?? []

        var returnArray = [CardData]()

        for relation in cardRelations {
            for _ in 0..<(relation.amount?.intValue ?? 0) {
                if let card = relation.card?.toCardData() {
                    returnArray.append(card)
                }
            }
        }

        return returnArray
    }
}
