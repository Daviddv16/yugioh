//
//  API.swift
//  flickr
//
//  Created by David Vieth on 4/7/17.
//  Copyright © 2017 David Vieth. All rights reserved.
//

import UIKit
import Alamofire

struct CardDataResponse: Decodable {
    let status: String
    let data: CardDataDecodable?
    let message: String?

    enum CodingKeys: String, CodingKey {
        case status
        case data
        case message
    }
}

struct CardDataDecodable: Decodable {
    let name: String
    let cardType: String
    let text: String

    let type: String?
    let level: Int?
    let attribute: String?
    let atk: Int?
    let def: Int?

    let property: String?

    func toCardData() -> CardData {
        if cardType == "monster" {
            return MonsterCard(data: self)
        } else {
            return SpellTrapCard(data: self)
        }
    }

    enum CodingKeys: String, CodingKey {
        case name
        case cardType = "card_type"
        case text
        case type
        case level
        case attribute = "family"
        case atk
        case def
        case property
    }
}

enum APIRouter {
	case getData(name: String)
	case getImage(name: String)

	var pathString: String? {
		let pathPrefix: String = "http://yugiohprices.com/api/"
		let path: String

		switch self {
		case let .getData(name):
			path = "card_data/" + name
		case let .getImage(name):
			path = "card_image/" + name
		}

        return (pathPrefix + path).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
	}

	/// Get path as a URL
	var url: URL? {
        guard let pathString = pathString else {
            return nil
        }

		return URL(string: pathString)
	}

	var method: String {
        switch self {
        case .getData,
             .getImage:
            return "GET"
        }
	}
}

func getCardData(for name: String,
                 finished: @escaping ((_ errorMessage: String?, _ card: CardDataDecodable?) -> Void)) {
    guard let urlPath = APIRouter.getData(name: name).pathString else {
        finished("Invalid URL Path", nil)
        return
    }

    AF.request(urlPath)
        .validate()
        .responseDecodable(of: CardDataResponse.self) { (response) in
            guard let value = response.value else {
                finished("Failed to get valid response", nil)
                print(response)
                return
            }

            guard value.status == "success" else {
                finished(value.message ?? "Invalid response", nil)
                return
            }

            finished(nil, value.data)
        }
}
