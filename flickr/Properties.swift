//
//  Config.swift
//  yugiohViewer
//
//  Created by David Vieth on 3/25/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import UIKit

class Config {
    enum Properties: String {
        case showDetails = "showDetails"
        case sortMethod = "sortName"
        case sortDirection = "sortDirection"
    }

    enum SortMethods: Int, CaseIterable {
        case byType = 0
        case byName = 1
        case byLevel = 2

        var displayValue: String {
            switch self {
            case .byName:
                return "Name"
            case .byType:
                return "Type"
            case .byLevel:
                return "Level"
            }
        }
    }

    static func getShowDetailsProperty() -> Bool {
        return (UserDefaults.standard.value(forKey: Properties.showDetails.rawValue) as? Bool) ?? true
    }

    static func setShowDetailsProperty(to value: Bool) {
        UserDefaults.standard.setValue(value, forKey: Properties.showDetails.rawValue)
    }

    static func getSortDirectionProperty() -> Bool {
        return (UserDefaults.standard.value(forKey: Properties.sortDirection.rawValue) as? Bool) ?? true
    }

    static func setSortDirectionProperty(to value: Bool) {
        UserDefaults.standard.setValue(value, forKey: Properties.sortDirection.rawValue)
    }

    static func getSortMethodProperty() -> SortMethods {
        let rawValue = (UserDefaults.standard.value(forKey: Properties.sortMethod.rawValue) as? Int) ?? -1
        return SortMethods(rawValue: rawValue) ?? .byName
    }

    static func setSortMethodProperty(to value: SortMethods) {
        UserDefaults.standard.setValue(value.rawValue, forKey: Properties.sortMethod.rawValue)
    }
}
