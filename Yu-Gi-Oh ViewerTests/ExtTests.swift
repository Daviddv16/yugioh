//
//  ExtTests.swift
//  Yu-Gi-Oh ViewerTests
//
//  Created by David Vieth on 3/29/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import XCTest
@testable import yugiohViewer

class ExtTests: XCTestCase {
    func testHexCodeInit() {
        let red = UIColor(hexCode: "#FF0000")

        guard let components = red.cgColor.components, components.count == 4 else {
            XCTFail("Unable to get color components")
            return
        }

        XCTAssert(components[0] == 1.0)
        XCTAssert(components[1] == 0.0)
        XCTAssert(components[2] == 0.0)
        XCTAssert(components[3] == 1.0)
    }

    func testHexCodeInitComplex() {
        let teal = UIColor(hexCode: "#008080")

        guard let components = teal.cgColor.components, components.count == 4 else {
            XCTFail("Unable to get color components")
            return
        }

        XCTAssert(components[0] == 0.0)
        XCTAssert((components[1] * 10000).rounded() / 100 == 50.2)
        XCTAssert((components[2] * 10000).rounded() / 100 == 50.2)
        XCTAssert(components[3] == 1.0)
    }

    func testHexCodeInitFailMissingHashtag() {
        let red = UIColor(hexCode: "FF0000")

        guard let components = red.cgColor.components, components.count == 2 else {
            XCTFail("Unable to get color components")
            return
        }

        XCTAssert(components[0] == 0.0)
        XCTAssert(components[1] == 1.0)
    }

    func testHexCodeInitFailWrongLength() {
        let red = UIColor(hexCode: "#FF000")

        guard let components = red.cgColor.components, components.count == 2 else {
            XCTFail("Unable to get color components")
            return
        }

        XCTAssert(components[0] == 0.0)
        XCTAssert(components[1] == 1.0)
    }

    func testHexCodeInitFailInvalidHexRed() {
        let red = UIColor(hexCode: "#FG0000")

        guard let components = red.cgColor.components, components.count == 2 else {
            XCTFail("Unable to get color components")
            return
        }

        XCTAssert(components[0] == 0.0)
        XCTAssert(components[1] == 1.0)
    }

    func testHexCodeInitFailInvalidHexBlue() {
        let red = UIColor(hexCode: "#FF0Q00")

        guard let components = red.cgColor.components, components.count == 2 else {
            XCTFail("Unable to get color components")
            return
        }

        XCTAssert(components[0] == 0.0)
        XCTAssert(components[1] == 1.0)
    }

    func testHexCodeInitFailInvalidHexGreen() {
        let red = UIColor(hexCode: "#FF00Q0")

        guard let components = red.cgColor.components, components.count == 2 else {
            XCTFail("Unable to get color components")
            return
        }

        XCTAssert(components[0] == 0.0)
        XCTAssert(components[1] == 1.0)
    }
}
