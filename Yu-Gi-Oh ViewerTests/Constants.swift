//
//  Constants.swift
//  Yu-Gi-Oh ViewerTests
//
//  Created by David Vieth on 4/7/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import Foundation
@testable import yugiohViewer

// MARK: Example Cards

var exampleSpellTrapCard: SpellTrapCard? {
    var cardData = [String: AnyObject]()

    cardData["name"] = "Monster Reborn" as AnyObject
    cardData["cardtype"] = "spell" as AnyObject
    cardData["text"] = "Target 1 monster in either GY; Special Summon it." as AnyObject
    cardData["property"] = "Normal" as AnyObject

    return SpellTrapCard(data: cardData)
}

var exampleMonsterCard: MonsterCard? {
    var cardData = [String: AnyObject]()

    cardData["name"] = "Mystic Elf" as AnyObject
    cardData["cardtype"] = "monster" as AnyObject
    cardData["text"] = "A delicate elf that lacks offsense" as AnyObject
    cardData["type"] = "spellcaster" as AnyObject
    cardData["level"] = 4 as AnyObject
    cardData["attribute"] = "light" as AnyObject
    cardData["atk"] = 800 as AnyObject
    cardData["def"] = 2000 as AnyObject

    return MonsterCard(data: cardData)
}

// MARK: Sorting Cards

var cardA: MonsterCard? {
    var cardData = [String: AnyObject]()

    cardData["name"] = "ABC" as AnyObject
    cardData["cardtype"] = "monster" as AnyObject
    cardData["text"] = "Test Card" as AnyObject
    cardData["type"] = "Dragon/Fusion" as AnyObject
    cardData["level"] = 4 as AnyObject
    cardData["attribute"] = "light" as AnyObject
    cardData["atk"] = 800 as AnyObject
    cardData["def"] = 2000 as AnyObject

    return MonsterCard(data: cardData)
}

var cardAA: MonsterCard? {
    var cardData = [String: AnyObject]()

    cardData["name"] = "BAC" as AnyObject
    cardData["cardtype"] = "monster" as AnyObject
    cardData["text"] = "Test Card" as AnyObject
    cardData["type"] = "Dragon/Fusion" as AnyObject
    cardData["level"] = 4 as AnyObject
    cardData["attribute"] = "light" as AnyObject
    cardData["atk"] = 800 as AnyObject
    cardData["def"] = 2000 as AnyObject

    return MonsterCard(data: cardData)
}

var cardB: MonsterCard? {
    var cardData = [String: AnyObject]()

    cardData["name"] = "XYZ" as AnyObject
    cardData["cardtype"] = "monster" as AnyObject
    cardData["text"] = "Test Card" as AnyObject
    cardData["type"] = "spellcaster" as AnyObject
    cardData["level"] = 4 as AnyObject
    cardData["attribute"] = "light" as AnyObject
    cardData["atk"] = 800 as AnyObject
    cardData["def"] = 2000 as AnyObject

    return MonsterCard(data: cardData)
}

var cardBB: MonsterCard? {
    var cardData = [String: AnyObject]()

    cardData["name"] = "ZYX" as AnyObject
    cardData["cardtype"] = "monster" as AnyObject
    cardData["text"] = "Test Card" as AnyObject
    cardData["type"] = "spellcaster" as AnyObject
    cardData["level"] = 4 as AnyObject
    cardData["attribute"] = "light" as AnyObject
    cardData["atk"] = 800 as AnyObject
    cardData["def"] = 2000 as AnyObject

    return MonsterCard(data: cardData)
}

var cardF: MonsterCard? {
    var cardData = [String: AnyObject]()

    cardData["name"] = "FGH" as AnyObject
    cardData["cardtype"] = "monster" as AnyObject
    cardData["text"] = "Test Card" as AnyObject
    cardData["type"] = "Dragon/Synchro" as AnyObject
    cardData["level"] = 4 as AnyObject
    cardData["attribute"] = "light" as AnyObject
    cardData["atk"] = 800 as AnyObject
    cardData["def"] = 2000 as AnyObject

    return MonsterCard(data: cardData)
}

var cardC: SpellTrapCard? {
    var cardData = [String: AnyObject]()

    cardData["name"] = "DEF" as AnyObject
    cardData["cardtype"] = "spell" as AnyObject
    cardData["text"] = "Target 1 monster in either GY; Special Summon it." as AnyObject
    cardData["property"] = "Normal" as AnyObject

    return SpellTrapCard(data: cardData)
}

var cardD: SpellTrapCard? {
    var cardData = [String: AnyObject]()

    cardData["name"] = "MNO" as AnyObject
    cardData["cardtype"] = "spell" as AnyObject
    cardData["text"] = "Target 1 monster in either GY; Special Summon it." as AnyObject
    cardData["property"] = "Normal" as AnyObject

    return SpellTrapCard(data: cardData)
}

var cardE: SpellTrapCard? {
    var cardData = [String: AnyObject]()

    cardData["name"] = "GHI" as AnyObject
    cardData["cardtype"] = "trap" as AnyObject
    cardData["text"] = "Target 1 monster in either GY; Special Summon it." as AnyObject
    cardData["property"] = "Normal" as AnyObject

    return SpellTrapCard(data: cardData)
}
