//
//  CardTableViewCellTests.swift
//  Yu-Gi-Oh ViewerTests
//
//  Created by David Vieth on 3/26/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import XCTest
@testable import yugiohViewer

class CardTableViewCellTests: XCTestCase {
    func testFormatFunctionSpellTrap() {
        let tableView = UITableView()
        let sampleDelegate = TestTableViewDataSource()
        tableView.dataSource = sampleDelegate
        tableView.delegate = sampleDelegate

        let cardCellNib = UINib(nibName: "CardTableViewCell", bundle: nil)
        tableView.register(cardCellNib, forCellReuseIdentifier: "CardTableViewCell")

        guard let cell = tableView.dequeueReusableCell(
                withIdentifier: "CardTableViewCell",
                for: IndexPath(row: 0, section: 0)) as? CardTableViewCell else {
            XCTFail("Failed to initialize cell as CardTableViewCell")
            return
        }

        guard let testSpellCard = exampleSpellTrapCard else {
            XCTFail("Failed to initialize Sample SpellTrapCard")
            return
        }
        cell.format(for: testSpellCard, withDetails: true)

        XCTAssert(cell.cardName.text == testSpellCard.name)
        XCTAssert(cell.backgroundColor == testSpellCard.displayColor)
        XCTAssert(cell.starImage.image == nil)
        XCTAssert(cell.cardLevel.text == "")
        XCTAssert(cell.atkDefLabel.text == "")
        XCTAssert(cell.attributeImage.image == testSpellCard.propertyIcon)
    }

    func testFormatFunctionMonster() {
        let tableView = UITableView()
        let sampleDelegate = TestTableViewDataSource()
        tableView.dataSource = sampleDelegate
        tableView.delegate = sampleDelegate

        let cardCellNib = UINib(nibName: "CardTableViewCell", bundle: nil)
        tableView.register(cardCellNib, forCellReuseIdentifier: "CardTableViewCell")

        guard let cell = tableView.dequeueReusableCell(
                withIdentifier: "CardTableViewCell",
                for: IndexPath(row: 0, section: 0)) as? CardTableViewCell else {
            XCTFail("Failed to initialize cell as CardTableViewCell")
            return
        }

        guard let testMonsterCard = exampleMonsterCard else {
            XCTFail("Failed to initialize Sample MonsterCard")
            return
        }
        cell.format(for: testMonsterCard, withDetails: true)

        XCTAssert(cell.cardName.text == testMonsterCard.name)
        XCTAssert(cell.backgroundColor == testMonsterCard.displayColor)
        XCTAssert(cell.starImage.image == UIImage(named: "CG_Star"))
        XCTAssert(cell.cardLevel.text == testMonsterCard.getLevel())
        XCTAssert(cell.atkDefLabel.text == testMonsterCard.getAtkDef())
        XCTAssert(cell.attributeImage.image == testMonsterCard.getAttributeImage())
    }

    func testFormatFunctionSpellTrapNoDetails() {
        Config.setShowDetailsProperty(to: false)

        let tableView = UITableView()
        let sampleDelegate = TestTableViewDataSource()
        tableView.dataSource = sampleDelegate
        tableView.delegate = sampleDelegate

        let cardCellNib = UINib(nibName: "CardTableViewCell", bundle: nil)
        tableView.register(cardCellNib, forCellReuseIdentifier: "CardTableViewCell")

        guard let cell = tableView.dequeueReusableCell(
                withIdentifier: "CardTableViewCell",
                for: IndexPath(row: 0, section: 0)) as? CardTableViewCell else {
            XCTFail("Failed to initialize cell as CardTableViewCell")
            return
        }

        guard let testSpellCard = exampleSpellTrapCard else {
            XCTFail("Failed to initialize Sample SpellTrapCard")
            return
        }
        cell.format(for: testSpellCard, withDetails: false)

        XCTAssert(cell.cardName.text == testSpellCard.name)
        XCTAssert(cell.backgroundColor == testSpellCard.displayColor)
        XCTAssert(cell.starImage.image == nil)
        XCTAssert(cell.cardLevel.text == "")
        XCTAssert(cell.atkDefLabel.text == "")
        XCTAssert(cell.attributeImage.image == nil)
    }

    func testFormatFunctionMonsterNoDetails() {
        Config.setShowDetailsProperty(to: false)

        let tableView = UITableView()
        let sampleDelegate = TestTableViewDataSource()
        tableView.dataSource = sampleDelegate
        tableView.delegate = sampleDelegate

        let cardCellNib = UINib(nibName: "CardTableViewCell", bundle: nil)
        tableView.register(cardCellNib, forCellReuseIdentifier: "CardTableViewCell")

        guard let cell = tableView.dequeueReusableCell(
                withIdentifier: "CardTableViewCell",
                for: IndexPath(row: 0, section: 0)) as? CardTableViewCell else {
            XCTFail("Failed to initialize cell as CardTableViewCell")
            return
        }

        guard let testMonsterCard = exampleMonsterCard else {
            XCTFail("Failed to initialize Sample MonsterCard")
            return
        }
        cell.format(for: testMonsterCard, withDetails: false)

        XCTAssert(cell.cardName.text == testMonsterCard.name)
        XCTAssert(cell.backgroundColor == testMonsterCard.displayColor)
        XCTAssert(cell.starImage.image == nil)
        XCTAssert(cell.cardLevel.text == "")
        XCTAssert(cell.atkDefLabel.text == "")
        XCTAssert(cell.attributeImage.image == nil)
    }
}

class TestTableViewDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
}

class TestCollectionViewDataSource: NSObject, UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return UICollectionViewCell()
    }
}
