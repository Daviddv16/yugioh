//
//  Yu_Gi_Oh_ViewerTests.swift
//  Yu-Gi-Oh ViewerTests
//
//  Created by David Vieth on 3/19/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import XCTest
@testable import yugiohViewer

class YuGiOhViewerTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}

class TestNavBarViewDelegate: NavBarViewDelegate {
    var backButtonCalled = false
    var plusButtonCalled = false

    func didClickBackButton() {
        backButtonCalled = true
    }

    func didClickPlusButton() {
        plusButtonCalled = true
    }

    func resetFunctions() {
        backButtonCalled = false
        plusButtonCalled = false
    }
}
