//
//  CardDetailsStackView.swift
//  Yu-Gi-Oh ViewerTests
//
//  Created by David Vieth on 4/6/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import XCTest
@testable import yugiohViewer

class CardDetailsStackViewTestCase: XCTestCase {

    func testSetMonsterCard() throws {
        let frame = CGRect(x: 0.0, y: 0.0, width: 200, height: 400)
        let cardDetailsStack = CardDetailsStackView(frame: frame)
        cardDetailsStack.setDetails(for: exampleMonsterCard!)

        XCTAssertEqual(cardDetailsStack.attackLabel?.text, "ATK:\n\(exampleMonsterCard!.atk)")
        XCTAssertEqual(cardDetailsStack.defenceLabel?.text, "DEF:\n\(exampleMonsterCard!.def)")
        XCTAssertEqual(cardDetailsStack.typeLabel?.text, exampleMonsterCard!.attribute.capitalized)
        XCTAssertEqual(cardDetailsStack.levelLabel?.text, "Level:\n\(exampleMonsterCard!.level)")
        XCTAssertEqual(cardDetailsStack.typesLabel?.text, "spellcaster")
    }

    func testSetSpellCard() throws {
        let frame = CGRect(x: 0.0, y: 0.0, width: 200, height: 400)
        let cardDetailsStack = CardDetailsStackView(frame: frame)
        cardDetailsStack.setDetails(for: exampleSpellTrapCard!)

        XCTAssertEqual(cardDetailsStack.attackLabel?.text, "")
        XCTAssertEqual(cardDetailsStack.defenceLabel?.text, "")
        XCTAssertEqual(cardDetailsStack.typeLabel?.text, "")
        XCTAssertEqual(cardDetailsStack.levelLabel?.text, "")
        XCTAssertEqual(cardDetailsStack.typesLabel?.text, "")
    }

}
