//
//  SortCardsTests.swift
//  Yu-Gi-Oh ViewerTests
//
//  Created by David Vieth on 4/1/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import XCTest
@testable import yugiohViewer

class SortCardsTests: XCTestCase {
    // MARK: Both Regular Monsters

    func testSortRegularMonsterCardsAlphabeticallyAscending() {
        guard let cardBB = cardBB, let cardB = cardB else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardB, cardBB, sortMethod: Config.SortMethods.byName, sortDirection: true)
        XCTAssert(isAFirst)
    }

    func testSortRegularMonsterCardsAlphabeticallyDescending() {
        guard let cardBB = cardBB, let cardB = cardB else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardB, cardBB, sortMethod: Config.SortMethods.byName, sortDirection: false)
        XCTAssert(!isAFirst)
    }

    func testSortRegularMonsterCardsTypeAscending() {
        guard let cardBB = cardBB, let cardB = cardB else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardB, cardBB, sortMethod: Config.SortMethods.byType, sortDirection: true)
        XCTAssert(isAFirst)
    }

    func testSortRegularMonsterCardsTypeDescending() {
        guard let cardBB = cardBB, let cardB = cardB else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardB, cardBB, sortMethod: Config.SortMethods.byType, sortDirection: false)
        XCTAssert(!isAFirst)
    }

    // MARK: One Extra One Regular Monster

    func testSortMonsterCardsAlphabeticallyAscending() {
        guard let cardA = cardA, let cardB = cardB else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardA, cardB, sortMethod: Config.SortMethods.byName, sortDirection: true)
        XCTAssert(isAFirst)
    }

    func testSortMonsterCardsAlphabeticallyDescending() {
        guard let cardA = cardA, let cardB = cardB else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardA, cardB, sortMethod: Config.SortMethods.byName, sortDirection: false)
        XCTAssert(!isAFirst)
    }

    func testSortMonsterCardsTypeAscending() {
        guard let cardA = cardA, let cardB = cardB else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardA, cardB, sortMethod: Config.SortMethods.byType, sortDirection: true)
        XCTAssert(!isAFirst)
    }

    func testSortMonsterCardsTypeDescending() {
        guard let cardA = cardA, let cardB = cardB else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardA, cardB, sortMethod: Config.SortMethods.byType, sortDirection: false)
        XCTAssert(isAFirst)
    }

    func testSortMonsterCardsAlphabeticallyAscending2() {
        guard let cardA = cardA, let cardB = cardB else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardB, cardA, sortMethod: Config.SortMethods.byName, sortDirection: true)
        XCTAssert(!isAFirst)
    }

    func testSortMonsterCardsAlphabeticallyDescending2() {
        guard let cardA = cardA, let cardB = cardB else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardB, cardA, sortMethod: Config.SortMethods.byName, sortDirection: false)
        XCTAssert(isAFirst)
    }

    func testSortMonsterCardsTypeAscending2() {
        guard let cardA = cardA, let cardB = cardB else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardB, cardA, sortMethod: Config.SortMethods.byType, sortDirection: true)
        XCTAssert(isAFirst)
    }

    func testSortMonsterCardsTypeDescending2() {
        guard let cardA = cardA, let cardB = cardB else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardB, cardA, sortMethod: Config.SortMethods.byType, sortDirection: false)
        XCTAssert(!isAFirst)
    }

    // MARK: Different Extra Cards

    func testSortExtraMonsterCardsAlphabeticallyAscending() {
        guard let cardA = cardA, let cardF = cardF else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardA, cardF, sortMethod: Config.SortMethods.byName, sortDirection: true)
        XCTAssert(isAFirst)
    }

    func testSorExtratMonsterCardsAlphabeticallyDescending() {
        guard let cardA = cardA, let cardF = cardF else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardA, cardF, sortMethod: Config.SortMethods.byName, sortDirection: false)
        XCTAssert(!isAFirst)
    }

    func testSortExtraMonsterCardsTypeAscending() {
        guard let cardA = cardA, let cardF = cardF else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardA, cardF, sortMethod: Config.SortMethods.byType, sortDirection: true)
        XCTAssert(isAFirst)
    }

    func testSortExtraMonsterCardsTypeDescending() {
        guard let cardA = cardA, let cardF = cardF else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardA, cardF, sortMethod: Config.SortMethods.byType, sortDirection: false)
        XCTAssert(!isAFirst)
    }

    // MARK: Same Extra Cards

    func testSortSameExtraMonsterCardsAlphabeticallyAscending() {
        guard let cardA = cardA, let cardAA = cardAA else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardA, cardAA, sortMethod: Config.SortMethods.byName, sortDirection: true)
        XCTAssert(isAFirst)
    }

    func testSorESamextratMonsterCardsAlphabeticallyDescending() {
        guard let cardA = cardA, let cardAA = cardAA else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardA, cardAA, sortMethod: Config.SortMethods.byName, sortDirection: false)
        XCTAssert(!isAFirst)
    }

    func testSortSameExtraMonsterCardsTypeAscending() {
        guard let cardA = cardA, let cardAA = cardAA else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardA, cardAA, sortMethod: Config.SortMethods.byType, sortDirection: true)
        XCTAssert(isAFirst)
    }

    func testSortSameExtraMonsterCardsTypeDescending() {
        guard let cardA = cardA, let cardAA = cardAA else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardA, cardAA, sortMethod: Config.SortMethods.byType, sortDirection: false)
        XCTAssert(!isAFirst)
    }

    // MARK: Spell and Trap

    func testSortSpellCardsAlphabeticallyAscending() {
        guard let cardC = cardC, let cardD = cardD else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardC, cardD, sortMethod: Config.SortMethods.byName, sortDirection: true)
        XCTAssert(isAFirst)
    }

    func testSortSpellCardsAlphabeticallyDescending() {
        guard let cardC = cardC, let cardD = cardD else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardC, cardD, sortMethod: Config.SortMethods.byName, sortDirection: false)
        XCTAssert(!isAFirst)
    }

    func testSortSpellCardsTypeAscending() {
        guard let cardC = cardC, let cardD = cardD else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardC, cardD, sortMethod: Config.SortMethods.byType, sortDirection: true)
        XCTAssert(isAFirst)
    }

    func testSortSpellCardsTypeDescending() {
        guard let cardC = cardC, let cardD = cardD else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardC, cardD, sortMethod: Config.SortMethods.byType, sortDirection: false)
        XCTAssert(!isAFirst)
    }

    func testSortSpellTrapCardsAlphabeticallyAscending() {
        guard let cardC = cardC, let cardE = cardE else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardC, cardE, sortMethod: Config.SortMethods.byName, sortDirection: true)
        XCTAssert(isAFirst)
    }

    func testSortSpellTrapCardsAlphabeticallyDescending() {
        guard let cardC = cardC, let cardE = cardE else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardC, cardE, sortMethod: Config.SortMethods.byName, sortDirection: false)
        XCTAssert(!isAFirst)
    }

    func testSortSpellTrapCardsAlphabeticallyAscending2() {
        guard let cardD = cardD, let cardE = cardE else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardD, cardE, sortMethod: Config.SortMethods.byName, sortDirection: true)
        XCTAssert(!isAFirst)
    }

    func testSortSpellTrapCardsAlphabeticallyDescending2() {
        guard let cardD = cardD, let cardE = cardE else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardD, cardE, sortMethod: Config.SortMethods.byName, sortDirection: false)
        XCTAssert(isAFirst)
    }

    func testSortSpellTrapCardsTypeAscending() {
        guard let cardC = cardC, let cardE = cardE else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardC, cardE, sortMethod: Config.SortMethods.byType, sortDirection: true)
        XCTAssert(isAFirst)
    }

    func testSortSpellTrapCardsTypeDescending() {
        guard let cardC = cardC, let cardE = cardE else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardC, cardE, sortMethod: Config.SortMethods.byType, sortDirection: false)
        XCTAssert(!isAFirst)
    }

    func testSortSpellTrapCardsTypeAscending2() {
        guard let cardD = cardD, let cardE = cardE else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardD, cardE, sortMethod: Config.SortMethods.byType, sortDirection: true)
        XCTAssert(isAFirst)
    }

    func testSortSpellTrapCardsTypeDescending2() {
        guard let cardD = cardD, let cardE = cardE else {
            XCTFail("Unable to initialize test data")
            return
        }

        let isAFirst = sortCards(cardD, cardE, sortMethod: Config.SortMethods.byType, sortDirection: false)
        XCTAssert(!isAFirst)
    }
}
