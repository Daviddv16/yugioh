//
//  TestNavBarViewDelegate.swift
//  Yu-Gi-Oh ViewerTests
//
//  Created by David Vieth on 3/19/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

//swiftlint:disable force_cast

import XCTest
@testable import yugiohViewer

class NavBarViewTests: XCTestCase {
    func testBackButtonDelegateMethodCalled() {
        let frame = CGRect(x: 0.0, y: 0.0, width: 150, height: 100)
        let navBarView = NavBarView(frame: frame)
        let testDelegate = TestNavBarViewDelegate()
        navBarView.delegate = testDelegate

        navBarView.pressBackBtn(
            sender: navBarView.backBtnStackView.gestureRecognizers?.first as! UITapGestureRecognizer
        )

        _ = XCTWaiter.wait(for: [expectation(description: "Wait for 1 second")], timeout: 1.0)
        XCTAssertTrue(testDelegate.backButtonCalled)
    }

    func testPlusButtonDelegateMethodCalled() {
        let frame = CGRect(x: 0.0, y: 0.0, width: 150, height: 100)
        let navBarView = NavBarView(frame: frame)
        let testDelegate = TestNavBarViewDelegate()
        navBarView.delegate = testDelegate

        navBarView.pressPlusBtn()
        XCTAssertTrue(testDelegate.plusButtonCalled)
    }

    func testDefaultTitle() {
        let frame = CGRect(x: 0.0, y: 0.0, width: 150, height: 100)
        let navBarView = NavBarView(frame: frame)

        XCTAssert(navBarView.titleLabel.text == "Controller")
    }

    func testPlusButtonIsHiddenByDefault() {
        let frame = CGRect(x: 0.0, y: 0.0, width: 150, height: 100)
        let navBarView = NavBarView(frame: frame)

        XCTAssert(navBarView.plusButton.isHidden)
        XCTAssert(!navBarView.plusButton.isEnabled)
    }

    func testShowPlusButton() {
        let frame = CGRect(x: 0.0, y: 0.0, width: 150, height: 100)
        let navBarView = NavBarView(frame: frame)

        navBarView.showPlusButton()

        XCTAssert(!navBarView.plusButton.isHidden)
        XCTAssert(navBarView.plusButton.isEnabled)
    }
}
