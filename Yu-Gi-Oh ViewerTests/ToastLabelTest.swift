//
//  ToastLabelTest.swift
//  Yu-Gi-Oh ViewerTests
//
//  Created by David Vieth on 3/29/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import XCTest
@testable import yugiohViewer

class ToastLabelTest: XCTestCase {
    func testLayoutSubview() {
        let frame = CGRect(x: 0.0, y: 0.0, width: 450, height: 500)
        let view = UIView(frame: frame)
        let toast = ToastLabel()
        toast.frame = CGRect(x: view.frame.size.width / 2 - 125,
                             y: view.frame.size.height - 150,
                             width: 250,
                             height: 35)
        view.addSubview(toast)
        view.layoutIfNeeded()
    }

    func testShowToast() {
        let frame = CGRect(x: 0.0, y: 0.0, width: 450, height: 500)
        let viewController = UIViewController()
        viewController.view.frame = frame

        viewController.showToast(message: "Test Message")

        _ = XCTWaiter.wait(for: [expectation(description: "Wait for 1 second")], timeout: 1.0)
    }
}
