//
//  CardCollectionViewCellTest.swift
//  Yu-Gi-Oh ViewerTests
//
//  Created by David Vieth on 3/29/21.
//  Copyright © 2021 David Vieth. All rights reserved.
//

import XCTest
@testable import yugiohViewer

class CardCollectionViewCellTest: XCTestCase {
    func testFormatFunction() {
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: 100, height: 100),
                                              collectionViewLayout: UICollectionViewFlowLayout())
        let sampleDelegate = TestCollectionViewDataSource()
        collectionView.dataSource = sampleDelegate
        collectionView.delegate = sampleDelegate

        let cardCellNib = UINib(nibName: "CardCollectionViewCell", bundle: nil)
        collectionView.register(cardCellNib, forCellWithReuseIdentifier: "CardCollectionViewCell")

        guard let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: "CardCollectionViewCell",
                for: IndexPath(row: 0, section: 0)) as? CardCollectionViewCell else {
            XCTFail("Failed to initialize cell as CardCollectionViewCell")
            return
        }

        guard let testSpellCard = exampleSpellTrapCard else {
            XCTFail("Failed to initialize Sample SpellTrapCard")
            return
        }
        cell.format(for: testSpellCard)
    }
}
